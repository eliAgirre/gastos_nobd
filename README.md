**Aplicacion de Java para un mayor control sobre la economia domestica.**

Es una aplicacion de escritorio de Java para aquellas personas que necesiten controlar sus gastos de la casa.

La aplicacion incluye insertar compras, insertar gastos, buscar producto, presupuesto e ingresos de la casa. Y como informacion adicional existe el ahorro mensual, los gastos fijos y recibir o enviar emails.

---