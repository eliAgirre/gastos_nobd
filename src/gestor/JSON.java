package gestor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import modelo.FormaCompra;
import modelo.FormaPago;
import modelo.Tiendas;
import modelo.TipoCompra;
import utilidades.Constantes; 

public class JSON {
	
	public JSON(){}
	
	public static String readfile(String filename) throws IOException {
		
		//BufferedReader reader = new BufferedReader(new FileReader(Constantes.RUTA_ARCHIVOS+filename));
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String json = "";
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = reader.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(Constantes.HTML_SALTO_LINEA);
		        line = reader.readLine();
		    }
		    json = sb.toString();
		} finally {
		    reader.close();
		}
		return json;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList parseJSON(String json, String columna) throws JSONException {
		
		//JSONObject object = new JSONObject(json); // this will get you the entire JSON node
		//JSONArray array = object.getJSONArray(nombre); // put in whatever your JSON data name here, this will get you an array of all the nodes
		JSONArray array = new JSONArray(json);

		ArrayList lista = new ArrayList();
		
		for(int i=0; i<array.length(); i++){ // loop through the nodes
		    JSONObject temp = array.getJSONObject(i);
		    lista.add(i,temp.getString(columna));
		    //System.out.println(lista.get(i));
		}
		
		return lista;
	}
	
	public static Tiendas[] leerTiendasJSON(String jsonArrayString) {
		
		Gson gson = new Gson();
		//Gson gson = new GsonBuilder().serializeNulls().create();
		 
		Tiendas[]  tiendasArray = new Tiendas[Constantes.MAX_TIENDAS];
		 
		tiendasArray = gson.fromJson(jsonArrayString, Tiendas[].class); 
		 
		return tiendasArray;
	}
	
	public static TipoCompra[] leerTipoCompraJSON(String jsonArrayString) {
		
		Gson gson = new Gson();
		
		TipoCompra[]  tipoCompraArray = new TipoCompra[Constantes.MAX_TIPO_COMPRA];
		 
		tipoCompraArray = gson.fromJson(jsonArrayString, TipoCompra[].class); 
		 
		return tipoCompraArray;
	}
	
	public static FormaCompra[] leerFormaCompraJSON(String jsonArrayString) {
		
		Gson gson = new Gson();
		
		FormaCompra[]  formaCompraArray = new FormaCompra[Constantes.MAX_FORMA_COMPRA];
		 
		formaCompraArray = gson.fromJson(jsonArrayString, FormaCompra[].class); 
		 
		return formaCompraArray;
	}

	public static FormaPago[] leerFormaPagoJSON(String jsonArrayString) {
		
		Gson gson = new Gson();
		
		FormaPago[]  formaPagoArray = new FormaPago[Constantes.MAX_FORMA_PAGO];
		 
		formaPagoArray = gson.fromJson(jsonArrayString, FormaPago[].class); 
		 
		return formaPagoArray;
	}
}
