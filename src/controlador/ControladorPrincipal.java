package controlador;

import vista.Principal;

/**
 * Controla y muestra la ventana principal.
 * 
 */
public class ControladorPrincipal {
	
	//Atributos de la clase
	private static Principal ventanaPrincipal;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorPrincipal() {
		//Instancia la clase Principal (vista)
		ventanaPrincipal=new Principal();
		// Coloca la ventana en el centro de la pantalla
		ventanaPrincipal.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaPrincipal.setVisible(true);
	} //Cierre del constructor
	
} // Cierre de la clase