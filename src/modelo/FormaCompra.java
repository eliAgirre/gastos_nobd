package modelo;

public class FormaCompra {
	
	// Atributos de la clase
	private int idFormaCompra;
	private String formaCompra;
	
	public FormaCompra(){}
	
	public FormaCompra(String s){}
	
	public FormaCompra(int idFormaCompra, String formaCompra) {
		
		this.idFormaCompra = idFormaCompra;
		this.formaCompra = formaCompra;
	} // Constructor

	// Getters
	public int getIdFormaPago() {
		return idFormaCompra;
	}

	public String getTipoPago() {
		return formaCompra;
	}

	// Setters
	public void setIdFormaPago(int idFormaCompra) {
		this.idFormaCompra = idFormaCompra;
	}

	public void setTipoPago(String formaCompra) {
		this.formaCompra = formaCompra;
	}

	// Sirve para que apareca en JComboBox el forma de compra.
	@Override
	public String toString() {
		
		return formaCompra;
	} // Cierre toString

} // Cierre clase