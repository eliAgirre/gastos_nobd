package vista;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.border.EmptyBorder; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla; 

public class VerComprasDiarias extends JFrame implements ActionListener{ 

		private static final long serialVersionUID = -8624753721794395368L;
		
		// Atributos de la clase 
        private JPanel contentPane; 
        private JComboBox<String> cbTienda; 
        private JComboBox<String> cbFormaCompra; 
        private JComboBox<String> cbFormaPago; 
        private JButton btnConsultar; 
        private JButton btnDiario; 
        private JButton btnMenu; 
        private double total = Constantes.DEFAULT_DOBLE_CERO; 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_COMPRAS]; 
        ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS); 
        
        // Atributos del resultados user 
        /*private String resultTienda; 
        private String resultFormaCompra; 
        private String resultFormaPago;*/
        
        public VerComprasDiarias(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 678, 609); // size 
            setTitle(Constantes.VISTA_VER_COMPRAS_DIARIAS); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la ventana 
            componentes(); 
            
            modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                
        } // cierra constructor 
        
        private void componentes(){ 
                
            // Layout 
            contentPane = new JPanel(); 
            contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); 
            setContentPane(contentPane); 
            contentPane.setLayout(null); 
            
            // Panel para visualizar y hacer scroll 
            scrollPane = new JScrollPane(); 
            scrollPane.setViewportBorder(null); 
            scrollPane.setEnabled(false); 
            scrollPane.setBounds(24, 153, 617, 303); 
            contentPane.add(scrollPane); 
            
            String[] campos = null; 
            
            if(!UtilesValida.esNulo(registros)) { 
                for(int i=0; i<registros.size(); i++){ 
                        
                    campos = registros.get(i).split(Constantes.PUNTO_COMA); 
                    total += Double.parseDouble(campos[7]); 
                } 
            } 
                    
            // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
            modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS); 
            
            // Se le pasa a JTable el modelo de tabla 
            tabla = new JTable(modelo); 
            UtilidadesTabla.anchoColumnasTablaCompras(tabla); 
            tabla.setEnabled(false); 
            tabla.setBorder(null); 
            scrollPane.setViewportView(tabla); 
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 554, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 323, 14, Constantes.LIT_INFO_VER+Constantes.PUNTOS)); // Aviso de seleccion 

            contentPane.add(Utilidades.obtenerLabel(24, 99, 88, 14, Constantes.LABEL_TIENDA+Constantes.PUNTOS+Constantes.ESPACIO)); // label tienda 
            
            cbTienda = Utilidades.obtenerCombo(112, 96, 126, 20, cbTienda); // combo tiendas 
            cbTienda.addActionListener((ActionListener)this); 
            contentPane.add(cbTienda); 
            
            contentPane.add(Utilidades.obtenerLabel(257, 96, 88, 14, Constantes.LABEL_FORMA_PAGO+Constantes.PUNTOS+Constantes.ESPACIO)); // label forma de pago 
            
            cbFormaPago = Utilidades.obtenerCombo(349, 93, 126, 20, cbFormaPago); // combo forma de pago 
            cbFormaPago.addActionListener((ActionListener)this); 
            contentPane.add(cbFormaPago); 
            
            contentPane.add(Utilidades.obtenerLabel(257, 60, 95, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.PUNTOS+Constantes.ESPACIO)); // label forma de compra 
            
            cbFormaCompra = Utilidades.obtenerCombo(350, 57, 126, 20, cbFormaCompra); // combo forma de compra 
            cbFormaCompra.addActionListener((ActionListener)this); 
            contentPane.add(cbFormaCompra); 
            
            contentPane.add(Utilidades.obtenerLabel(422, 481, 50, 14, Constantes.LABEL_TOTAL+Constantes.PUNTOS+Constantes.ESPACIO)); // label total 
            
            contentPane.add(Utilidades.obtenerLabel(470, 481, 78, 14, String.valueOf(total))); // label importe 
            
            btnConsultar = Utilidades.obtenerBoton(500, 54, 140, 59, Constantes.BTN_CONSULTAR, btnConsultar); // boton consultar 
            btnConsultar.addActionListener((ActionListener)this); 
            btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnConsultar); 
            
            btnDiario = Utilidades.obtenerBoton(365, 528, 95, 41, Constantes.BTN_DIARIO, btnDiario); // boton diario 
            btnDiario.addActionListener((ActionListener)this); 
            btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DIARY+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnDiario); 
            
            btnMenu = Utilidades.obtenerBoton(220, 528, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
                
        } // Cierre componentes 
        
        private void limpiar(){ 
                
            cbTienda.setSelectedItem(0); 
            cbFormaCompra.setSelectedItem(0); 
            cbFormaPago.setSelectedItem(0); 
                
        } // Cierre limpiar 

        @SuppressWarnings("unused") 
        @Override 
        public void actionPerformed(ActionEvent evento) { 
                
           /* if(evento.getSource()==cbTienda){ 
                    
                if(cbTienda.isValid()==true) 
                        resultTienda = cbTienda.getSelectedItem().toString(); 
            } 
            
            if(evento.getSource()==cbFormaCompra){ 
                    
                if(cbFormaCompra.isValid()==true) 
                        resultFormaCompra = cbFormaCompra.getSelectedItem().toString(); 
            } 
            
            if(evento.getSource()==cbFormaPago){ 
                    
                if(cbFormaPago.isValid()==true) 
                        resultFormaPago = cbFormaPago.getSelectedItem().toString(); 
            } */
            
            if(evento.getSource()==btnConsultar){ 
                    
                limpiar(); 
                    
            } 
            
            if(evento.getSource()==btnDiario){ 
                    
                limpiar(); 
            } 
            
            if(evento.getSource()==btnMenu){ 
                    
                  ControladorPrincipal controladorPrincipal = new ControladorPrincipal(); 
                  setVisible(false); 
            } 
                
        } // cierra actionPerformed 

} // cierre de clase
