package vista;

import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

import utilidades.Constantes;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class Principal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	
		// items del menu
		private JMenuItem itemPresu;
		private JMenuItem itemIngreso;
		private JMenuItem itemEditarPresu;
		private JMenuItem itemEditarIngreso;
		private JMenuItem itemEditarGastosFijos;
		private JMenuItem itemProducto;
		private JMenuItem itemPresupuestos;
		private JMenuItem itemIngresos;
		private JMenuItem itemVerGastos;
		private JMenuItem itemConcretos;
		private JMenuItem itemVerGastosFijos;
		private JMenuItem itemSemanal;
		private JMenuItem itemComprasLog;
		private JMenuItem itemFijosLog;
		private JMenuItem itemGraficoComida;
		private JMenuItem itemGraficoOnline;
		private JMenuItem itemGraficoGastoFijo;
		private JMenuItem itemCrearMail;
		private JMenuItem itemRecibirMail;
		
		// accesos directos - botones
		private JButton btnCompra;
		private JButton btnFijo;
		private JButton btnEditarCompras;
		//private JButton btnDiario;		
		private JButton btnFechas;
		private JButton btnFijos;
		private JButton btnAhorroMensual;
		
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Principal(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 574, 326); // size
		setTitle(Constantes.VISTA_CONTABILIDAD); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // La equis de la ventana detiene la aplicacion
		
		// Carga los componentes de la ventana
		componentes();
		
	} // constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Principal.
     *  
     */
	public void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 272, 79, 14));
		
		//contenedor de menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 581, 21);
		contentPane.add(menuBar);
		
		
		
		/*************************************************************/
		/************************* ANADIR ****************************/
		/*************************************************************/
		
		// menu + items del menu
		JMenu mnAnadir = new JMenu(Constantes.ANADIR); 
		menuBar.add(mnAnadir);
		
		itemPresu = new JMenuItem(Constantes.MENU_PRESUPUESTO);
		itemPresu.addActionListener((ActionListener)this);
		mnAnadir.add(itemPresu);
		
		itemIngreso = new JMenuItem(Constantes.MENU_INGRESO);
		itemIngreso.addActionListener((ActionListener)this);
		mnAnadir.add(itemIngreso);
		
		
		
		/*************************************************************/
		/************************* EDITAR ****************************/
		/*************************************************************/
		
		JMenu mnEditar = new JMenu(Constantes.EDITAR);
		menuBar.add(mnEditar);
		
		itemEditarPresu = new JMenuItem(Constantes.MENU_PRESUPUESTOS);
		itemEditarPresu.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarPresu);
		
		itemEditarIngreso = new JMenuItem(Constantes.MENU_INGRESOS);
		itemEditarIngreso.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarIngreso);
		
		itemEditarGastosFijos = new JMenuItem(Constantes.MENU_GASTOS_FIJOS);
		itemEditarGastosFijos.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarGastosFijos);
		
		
		
		/*************************************************************/
		/************************* BUSCAR ****************************/
		/*************************************************************/
		
		JMenu mnBuscar = new JMenu(Constantes.BUSCAR);
		menuBar.add(mnBuscar);
		
		itemProducto = new JMenuItem(Constantes.MENU_PRODUCTO);
		itemProducto.addActionListener((ActionListener)this);
		mnBuscar.add(itemProducto);
		
		
		
		/*************************************************************/
		/*************************   VER   ***************************/
		/*************************************************************/
		
		JMenu mnVer = new JMenu(Constantes.VER);
		menuBar.add(mnVer);
		
		itemPresupuestos = new JMenuItem(Constantes.MENU_PRESUPUESTOS);
		itemPresupuestos.addActionListener((ActionListener)this);
		mnVer.add(itemPresupuestos);
		
		itemIngresos = new JMenuItem(Constantes.MENU_INGRESOS);
		itemIngresos.addActionListener((ActionListener)this);
		mnVer.add(itemIngresos);
		
		itemVerGastos = new JMenuItem(Constantes.MENU_COMPRAS_DIARIAS);
		itemVerGastos.addActionListener((ActionListener)this);
		mnVer.add(itemVerGastos);
		
		itemConcretos = new JMenuItem(Constantes.MENU_COMPRAS_CONCRETAS);
		itemConcretos.addActionListener((ActionListener)this);
		mnVer.add(itemConcretos);
		
		itemVerGastosFijos = new JMenuItem(Constantes.MENU_GASTOS_FIJOS);
		itemVerGastosFijos.addActionListener((ActionListener)this);
		mnVer.add(itemVerGastosFijos);
		
		
		
		/*************************************************************/
		/************************* AHORRO ****************************/
		/*************************************************************/
		
		JMenu mnAhorro = new JMenu(Constantes.MENU_AHORRO);
		menuBar.add(mnAhorro);
		
		itemSemanal = new JMenuItem(Constantes.MENU_SEMANAL);
		itemSemanal.addActionListener((ActionListener)this);
		mnAhorro.add(itemSemanal);
		
		
		
		/*************************************************************/
		/************************* FICHERO ***************************/
		/*************************************************************/
		
		JMenu mnFichero = new JMenu(Constantes.MENU_FICHERO);
		menuBar.add(mnFichero);
		
		itemComprasLog = new JMenuItem(Constantes.MENU_COMPRAS_LOG);
		itemComprasLog.addActionListener((ActionListener)this);
		mnFichero.add(itemComprasLog);
		
		itemFijosLog = new JMenuItem(Constantes.MENU_FIJOS_LOG);
		itemFijosLog.addActionListener((ActionListener)this);
		mnFichero.add(itemFijosLog);
		
		
				
		/*************************************************************/
		/************************* GRAFICO ***************************/
		/*************************************************************/
		
		JMenu mnGrafico = new JMenu(Constantes.MENU_GRAFICO);
		menuBar.add(mnGrafico);
		
		itemGraficoComida = new JMenu(Constantes.MENU_COMIDA);
		itemGraficoComida.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoComida);
		
		itemGraficoOnline = new JMenu(Constantes.MENU_TARJETA_NET);
		itemGraficoOnline.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoOnline);		
		
		itemGraficoGastoFijo = new JMenuItem(Constantes.MENU_GASTO_FIJOS);
		itemGraficoGastoFijo.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoGastoFijo);
		
		
		
		/*************************************************************/
		/************************* EMAIL *****************************/
		/*************************************************************/
				
		JMenu mnEmail = new JMenu(Constantes.MENU_EMAIL);
		menuBar.add(mnEmail);
		
		itemCrearMail = new JMenu(Constantes.MENU_ENVIAR);
		itemCrearMail.addActionListener((ActionListener)this);
		mnEmail.add(itemCrearMail);
		
		itemRecibirMail = new JMenuItem(Constantes.MENU_RECIBIR);
		itemRecibirMail.addActionListener((ActionListener)this);
		mnEmail.add(itemRecibirMail);
		
		
		
		/*************************************************************/
		/************************* BOTONES ***************************/
		/*************************************************************/
		
		// Accesos directos - botones
		btnCompra = Utilidades.obtenerBoton(10, 75, 152, 76, Constantes.ACCESO_COMPRA, btnCompra);
		btnCompra.addActionListener((ActionListener)this);
		btnCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnCompra);	
		
		btnFijo = Utilidades.obtenerBoton(202, 75, 152, 76, Constantes.ACCESO_FIJO, btnFijo);
		btnFijo.addActionListener((ActionListener)this);
		btnFijo.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFijo);
		
		btnEditarCompras = Utilidades.obtenerBoton(392, 75, 152, 76, Constantes.ACCESO_COMPRAS, btnEditarCompras);
		btnEditarCompras.addActionListener((ActionListener)this);
		btnEditarCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)));
		contentPane.add(btnEditarCompras);
		
		/*btnDiario = Utilidades.obtenerBoton(10, 174, 152, 76, Constantes.ACCESO_DIARIO, btnDiario);
		btnDiario.addActionListener((ActionListener)this);
		btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnDiario);*/
		
		btnFechas = Utilidades.obtenerBoton(10, 174, 152, 76, Constantes.ACCESO_FECHAS, btnFechas);
		btnFechas.addActionListener((ActionListener)this);
		btnFechas.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFechas);
		
		btnFijos = Utilidades.obtenerBoton(202, 174, 152, 76, Constantes.ACCESO_FIJOS, btnFijos);
		btnFijos.addActionListener((ActionListener)this);
		btnFijos.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFijos);
		
		btnAhorroMensual = Utilidades.obtenerBoton(392, 174, 152, 76, Constantes.MENU_AHORRO, btnAhorroMensual);
		btnAhorroMensual.addActionListener((ActionListener)this);
		btnAhorroMensual.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)));
		contentPane.add(btnAhorroMensual);
		
	} // Cierre componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en menu Agregar Presupuesto
		if(evento.getSource()==itemPresu ){ 

			AnadirPresupuesto ventanaPresupuesto=new AnadirPresupuesto();
			ventanaPresupuesto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaPresupuesto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Ingreso
		if(evento.getSource()==itemIngreso){

			AnadirIngreso ventanaAnadirIngreso=new AnadirIngreso();
			ventanaAnadirIngreso.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirIngreso.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton Agregar Compra
		if(evento.getSource()==btnCompra){ 

			AnadirCompra ventanaCompra=new AnadirCompra();
			ventanaCompra.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCompra.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton Agregar Fijo
		if(evento.getSource()==btnFijo){ 

			AnadirGastoFijo ventanaGastoFijo=new AnadirGastoFijo();
			ventanaGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}	
		// Si hace clic en boton Editar Compras
		if(evento.getSource()==btnEditarCompras){

			TablaCompras ventanaTablaCompras=new TablaCompras();
			ventanaTablaCompras.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaCompras.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar presupuesto
		if(evento.getSource()==itemEditarPresu){

			TablaPresupuestos ventanaTablaPresupuestos=new TablaPresupuestos();
			ventanaTablaPresupuestos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaPresupuestos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar Ingreso
		if(evento.getSource()==itemEditarIngreso){

			TablaIngresos ventanaTablaIngresos=new TablaIngresos();
			ventanaTablaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaIngresos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar Gasto Fijo
		if(evento.getSource()==itemEditarGastosFijos){

			TablaGastosFijos ventanaTablaGastosFijos=new TablaGastosFijos();
			ventanaTablaGastosFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaGastosFijos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Buscar producto
		if(evento.getSource()==itemProducto){

			BuscarProducto ventanaBuscarProducto=new BuscarProducto();
			ventanaBuscarProducto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaBuscarProducto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ver presupuestos
		if(evento.getSource()==itemPresupuestos ){ 

			VerPresupuestos ventanaPresupuestos=new VerPresupuestos();
			ventanaPresupuestos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaPresupuestos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ver ingresos
		if(evento.getSource()==itemIngresos){

			VerIngresos ventanaIngresos=new VerIngresos();
			ventanaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaIngresos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		} /*
		// Si hace clic en boton Gastos Diarios
		if(
		//evento.getSource()==btnDiario || 
		 evento.getSource()==itemVerGastos){ 

			VerDiario ventanaDiario=new VerDiario();
			ventanaDiario.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaDiario.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}*/
		// Se puede ver los gastos concretos entre fechas o no
		if(evento.getSource()==btnFechas ||evento.getSource()==itemConcretos){ 

			VerComprasConcretas ventanaConcretos=new VerComprasConcretas();
			ventanaConcretos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaConcretos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		} 
		// Si hace clic en boton Gastos Fijos
		if(evento.getSource()==btnFijos || evento.getSource()==itemVerGastosFijos){

			VerGastosFijos ventanaVerGastosFijo=new VerGastosFijos();
			ventanaVerGastosFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaVerGastosFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ahorro semanal 
		if(evento.getSource()==itemSemanal){ 

			VerAhorroSemanal ventanaSemanal=new VerAhorroSemanal();
			ventanaSemanal.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaSemanal.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}/*
		// Si hace clic en el boton Ahorro mensual
		if(evento.getSource()==btnAhorroMensual){ 

			VerMensual ventanaMensual=new VerMensual();
			ventanaMensual.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaMensual.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}		
		*/
		// Si hace clic en menu Compras Log
		if(evento.getSource()==itemComprasLog){
			
			try{
				
				String texto = utilidades.FicherosLog.leerFicheroLog(Constantes.FICHERO_COMPRAS);
				
				if(!UtilesValida.esNulo(texto)) {
					
		
					LogCompras ventanaLogCompras=new LogCompras(texto);
		
					ventanaLogCompras.setLocationRelativeTo(null);
					// Hace visible la ventana
					ventanaLogCompras.setVisible(true);
			  		// Deaparece esta ventana
			  		setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_VACIO);
				}
			}
			catch (IOException eoi) {
				JOptionPane.showMessageDialog(null, eoi.getMessage());
			}			

		}
		
		// Si hace clic en menu Gastos Fijos Log
		if(evento.getSource()==itemFijosLog){
	  		
			try{
				
				String texto = utilidades.FicherosLog.leerFicheroLog(Constantes.FICHERO_GASTOS_FIJOS);
				
				if(!UtilesValida.esNulo(texto)) {
					
					LogGastosFijos ventanaLogGastosFijos=new LogGastosFijos(texto);
		
					ventanaLogGastosFijos.setLocationRelativeTo(null);
					// Hace visible la ventana
					ventanaLogGastosFijos.setVisible(true);
			  		// Deaparece esta ventana
			  		setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_VACIO);
				}
			}
			catch (IOException eoi) {
				JOptionPane.showMessageDialog(null, eoi.getMessage());
			}
		}
		/*
		// Si hace clic en menu Grafico de comida
		if(evento.getSource()==itemGraficoComida){

			GraficoComida ventanaGraficoComida=new GraficoComida();
			ventanaGraficoComida.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoComida.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico de tarjetas
		if(evento.getSource()==itemGraficoOnline){

			GraficoOnline ventanaGraficoOnline=new GraficoOnline();
			ventanaGraficoOnline.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoOnline.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico de gastos fijos
		if(evento.getSource()==itemGraficoGastoFijo){

			GraficoGastosFijos ventanaGraficoGastoFijo=new GraficoGastosFijos();
			ventanaGraficoGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Enviar email
		if(evento.getSource()==itemCrearMail){

			EnviarMail ventanaCrearMail=new EnviarMail();
			ventanaCrearMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCrearMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Recibir email
		if(evento.getSource()==itemRecibirMail){

			RecibirMail ventanaRecibirMail=new RecibirMail();
			ventanaRecibirMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaRecibirMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}*/
		
	} // Cierre actionPerfomed
} // Cierre de clase