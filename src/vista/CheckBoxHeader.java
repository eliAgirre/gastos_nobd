package vista;

import java.awt.Component; 
import java.awt.event.ItemListener; 
import java.awt.event.MouseEvent; 
import java.awt.event.MouseListener; 

import javax.swing.JCheckBox; 
import javax.swing.JTable; 
import javax.swing.UIManager; 
import javax.swing.table.JTableHeader; 
import javax.swing.table.TableColumnModel; 

public class CheckBoxHeader extends JCheckBox implements MouseListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos 
        protected CheckBoxHeader rendererComponent; 
        protected int column; 
        protected boolean mousePressed = false; 
        
        public CheckBoxHeader(ItemListener itemListener) { 
                
            rendererComponent = this; 
            rendererComponent.addItemListener(itemListener); 
                
        } // Constructor 
        
        // Getter 
        public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column) { 
                
            if (table != null) { 
                JTableHeader header = table.getTableHeader(); 
                
                if (header != null) { 
                    rendererComponent.setForeground(header.getForeground()); 
                    rendererComponent.setBackground(header.getBackground()); 
                    rendererComponent.setFont(header.getFont()); 
                    header.addMouseListener(rendererComponent); 
                } 
            } 
            
            setColumn(column); 
            rendererComponent.setText("Seleccionar"); 
            setBorder(UIManager.getBorder("TableHeader.cellBorder")); 
            
            return rendererComponent; 
        } 
        
        protected void setColumn(int column) { 
            this.column = column; 
        } 
        
        public int getColumn() { 
            return column; 
        } 
        
        // Eventos 
        protected void handleClickEvent(MouseEvent e) { 
        	
            if (mousePressed){ 
                    
                mousePressed=false; 
                
                JTableHeader header = (JTableHeader)(e.getSource()); 
                JTable tableView = header.getTable(); 
                TableColumnModel columnModel = tableView.getColumnModel(); 
                
                int viewColumn = columnModel.getColumnIndexAtX(e.getX()); 
                int column = tableView.convertColumnIndexToModel(viewColumn); 
                
                if (viewColumn == this.column && e.getClickCount() == 1 && column != -1) { 
                    doClick(); 
                } 
            } 
        } // handleClickEvent 
  
        public void mouseClicked(MouseEvent e) { 
                
            handleClickEvent(e); 
            ((JTableHeader)e.getSource()).repaint(); 
                
        } //mouseClicked 
        
        public void mousePressed(MouseEvent e) { mousePressed = true; } // mousePressed 
        
        public void mouseReleased(MouseEvent e) {} 

        public void mouseEntered(MouseEvent e) {} 

        public void mouseExited(MouseEvent e) {} 

} // clase

