package vista;

import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import com.toedter.calendar.JDateChooser;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class EditarCompra extends JFrame implements ActionListener, PropertyChangeListener{
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JButton btnVolver;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JTextField txtID;
	private static JTextField txtProducto;
	private static JTextField txtImporte;
	private JDateChooser dateChooser;
	private static JComboBox<String> cbTiendas;
	private static JComboBox<String> cbFormaCompra;
	private static JComboBox<String> cbFormaPago;
	
	// Atributos del resultados user
	private int idCompra;
	private String resultFecha;
	private String resultTienda;
	private String resultProducto;
	private Double resultImporte;
	private String resultFormaPago;
	private String resultFormaCompra;
	
	public EditarCompra(String id, String fecha, String tienda, String producto, String importe, String formaCompra, String formaPago){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 638, 370); // size
		setTitle(Constantes.VISTA_EDITAR_COMPRA); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes(id, producto, importe);
		
		if(UtilesValida.esNumero(id))
			idCompra = Integer.valueOf(id);
		
		// LISTA DE TIENDAS
		if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTiendas) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);
		else
			resultTienda = Utilidades.getItem( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTiendas, tienda).toString(); // muestra el item previamente elegido
		
		// LISTA FORMA COMPRA		
		if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_COMPRA), cbFormaCompra) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);
		else
			resultFormaCompra = Utilidades.getItem( Utilidades.getLista(Constantes.MODELO_FORMA_COMPRA), cbFormaCompra, formaCompra).toString(); // muestra el item previamente elegido
		
		
		// LISTA FORMA DE PAGO	
		if(UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_PAGO), cbFormaPago) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_PAGO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);
		else
			resultFormaPago = Utilidades.getItem( Utilidades.getLista(Constantes.MODELO_FORMA_PAGO), cbFormaPago, formaPago).toString(); // muestra el item previamente elegido
						
		if(UtilesValida.esFecha(fecha))
			dateChooser.setDate(Utilidades.parseaFecha(fecha));

	} // Constructor
	
	private void componentes(String idCompra, String producto, String importe){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Labels + textFields + comboBox + buttons
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 567, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_INFO_MODIF)); // Aviso de modificacion
		
		contentPane.add(Utilidades.obtenerLabel(337, 36, 48, 14, Constantes.LABEL_ID+Constantes.ESPACIO+Constantes.PUNTOS)); // Label id 
		
		txtID = Utilidades.obtenerTextField(444, 33, 164, 20, 10, txtID, idCompra, false); // txtID
		txtID.addActionListener((ActionListener)this);
		contentPane.add(txtID);
		
		contentPane.add(Utilidades.obtenerLabel(48, 81, 88, 14, Constantes.LABEL_FECHA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha compra
		
		dateChooser = Utilidades.obtenerFecha(159, 78, 126, 20, dateChooser); // fecha compra
		dateChooser.addPropertyChangeListener(Constantes.INPUT_FECHA, (PropertyChangeListener) this);
		contentPane.add(dateChooser);
		
		contentPane.add(Utilidades.obtenerLabel(48, 125, 88, 14, Constantes.LABEL_TIENDA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label tienda
		
		cbTiendas = Utilidades.obtenerCombo(159, 122, 164, 20, cbTiendas); // combo tiendas
		cbTiendas.addActionListener((ActionListener)this);
		contentPane.add(cbTiendas);
		
		contentPane.add(Utilidades.obtenerLabel(48, 172, 95, 14, Constantes.LABEL_PRODUCTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label producto
		
		txtProducto = Utilidades.obtenerTextField(159, 169, 164, 20, 10, txtProducto, producto, true); // txtProducto
		txtProducto.addActionListener((ActionListener)this);
		contentPane.add(txtProducto);
		
		contentPane.add(Utilidades.obtenerLabel(339, 172, 95, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // Label importe
		
		txtImporte = Utilidades.obtenerTextField(444, 169, 164, 20, 10, txtImporte, importe, true); // txtImporte
		txtImporte.addActionListener((ActionListener)this);
		contentPane.add(txtImporte);
		
		contentPane.add(Utilidades.obtenerLabel(339, 125, 95, 14, Constantes.LABEL_FORMA_PAGO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label forma de pago
		
		cbFormaPago = Utilidades.obtenerCombo(444, 122, 164, 20, cbFormaPago); // combo forma de pago
		cbFormaPago.addActionListener((ActionListener)this);
		contentPane.add(cbFormaPago);
		
		contentPane.add(Utilidades.obtenerLabel(345, 75, 126, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label forma de compra
		
		cbFormaCompra = Utilidades.obtenerCombo(444, 75, 164, 20, cbFormaCompra); // combo forma de compra
		cbFormaCompra.addActionListener((ActionListener)this);
		contentPane.add(cbFormaCompra);
		
		btnVolver = Utilidades.obtenerBoton(126, 274, 108, 41, Constantes.BTN_VOLVER, btnVolver); // boton volver
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_BACK+Constantes.EXTENSION_PNG)));
		contentPane.add(btnVolver); 
		
		btnEditar = Utilidades.obtenerBoton(261, 274, 108, 41, Constantes.BTN_EDITAR, btnEditar); // boton editar
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT2+Constantes.EXTENSION_PNG)));
		contentPane.add(btnEditar); 
		
		btnBorrar = Utilidades.obtenerBoton(402, 274, 111, 41, Constantes.BTN_BORRAR, btnBorrar); // boton borrar
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DELETE+Constantes.EXTENSION_PNG)));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {

		if(evento.getSource()==cbTiendas){
			
			if(cbTiendas.isValid()){
				resultTienda=cbTiendas.getSelectedItem().toString();
			}
		}

		if(evento.getSource()==cbFormaCompra){
			
			if(cbFormaCompra.isValid()){
				resultFormaCompra=cbFormaCompra.getSelectedItem().toString();
			}
		}

		if(evento.getSource()==cbFormaPago){
			
			if(cbFormaPago.isValid()){
				resultFormaPago=cbFormaPago.getSelectedItem().toString();
			}
		}

		if(evento.getSource()==btnVolver){

			TablaCompras ventanaTablaCompras=new TablaCompras();
			ventanaTablaCompras.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaCompras.setVisible(true);
			// Desaparece esta ventana
	  		setVisible(false);
		}

		if(evento.getSource()==btnEditar){
			
			if( !UtilesValida.esNulo(cbTiendas.getSelectedItem()) )
				resultTienda=cbTiendas.getSelectedItem().toString();
			
			if( !UtilesValida.esNulo(cbFormaCompra.getSelectedItem()) )
				resultFormaCompra=cbFormaCompra.getSelectedItem().toString();
			
			if( !UtilesValida.esNulo(cbFormaPago.getSelectedItem()) )
				resultFormaPago=cbFormaPago.getSelectedItem().toString();
			
			boolean validar = false;	
			
			validar = utilidades.ValidacionesComun.validarCamposCompra(resultFecha, resultTienda, txtProducto.getText(), txtImporte.getText(), resultFormaCompra, resultFormaPago, txtProducto, txtImporte);
			
			if(validar){
				
				resultImporte = Double.valueOf(txtImporte.getText());
				resultProducto = txtProducto.getText();

				// Se obtiene la ultima linea del el fichero log
				String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_COMPRAS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) {
						
						utilidades.FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[1], Integer.valueOf(txtID.getText()), resultFecha, resultTienda, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago, resultProducto);
						
						ArrayList<String> listaCompras = FicherosRegistros.editarListaCompras(txtID.getText(), resultFecha, resultTienda, resultProducto, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago);
						
						if(!UtilesValida.esNula(listaCompras)) {
							
							if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_COMPRAS)) {
								
								if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_COMPRAS)) {
									
									FicherosRegistros.insertarRegistros(listaCompras, Constantes.FICHERO_COMPRAS);
									
									JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_MODI);

							  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

							  		setVisible(false); // Desparece esta ventana
								}
								else 
									JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
							}
							else
								JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO);
						}
						else
							JOptionPane.showMessageDialog(null, Constantes.ERROR_MODI);
					}
				}
				
			}
			
		} // editar
		
        if(evento.getSource()==btnBorrar){ 
            
            // Se obtiene la ultima linea del el fichero log 
            String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_COMPRAS); 
            
            if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                    
                if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) { 
                	
        			if( !UtilesValida.esNulo(cbTiendas.getSelectedItem()) )
        				resultTienda=cbTiendas.getSelectedItem().toString();
        			
        			if( !UtilesValida.esNulo(cbFormaCompra.getSelectedItem()) )
        				resultFormaCompra=cbFormaCompra.getSelectedItem().toString();
        			
        			if( !UtilesValida.esNulo(cbFormaPago.getSelectedItem()) )
        				resultFormaPago=cbFormaPago.getSelectedItem().toString();
                	
                	boolean validar = false;
                	
                	validar = utilidades.ValidacionesComun.validarCamposCompra(resultFecha, resultTienda, txtProducto.getText(), txtImporte.getText(), resultFormaCompra, resultFormaPago, txtProducto, txtImporte);
                        
                    if(validar) {
                    	
                    	resultProducto = txtProducto.getText();
                    	resultImporte = Double.valueOf(txtImporte.getText());
                    
                    	FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[2], idCompra, resultFecha, resultTienda, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago, resultProducto); 
                        
                        ArrayList<String> listaCompras = FicherosRegistros.borrarRegistroCompra(String.valueOf(idCompra)); 
                        
                        if(!UtilesValida.esNula(listaCompras)) { 
                                
                            if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_COMPRAS)) {
                                    
                                if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_COMPRAS)) { 
                                        
                                    FicherosRegistros.insertarRegistros(listaCompras, Constantes.FICHERO_COMPRAS); 
                                    
                                    JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_BAJA); 

                                    ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                    setVisible(false); // Desparece esta vemtana 
                                } 
                                else 
                                	JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
                            } 
                            else
                            	JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO); 
                        } 
                        else  
                        	JOptionPane.showMessageDialog(null, Constantes.ERROR_BAJA);       
                    }      
                } 
            } 
        } // borrar 

	} // Cierre del metodo actionPerformed
	
	@Override
	public void propertyChange(PropertyChangeEvent evento) {
		
		if (evento.getPropertyName().equals(Constantes.INPUT_FECHA)) {
			// Se le da formato a la fecha obtenida
			resultFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateChooser.getDate());
        }	
	} // cierre propertyChange
	
	// getters
	public static JTextField getTxtProducto() {
		return txtProducto;
	}

	public static JTextField getTxtImporte() {
		return txtImporte;
	}
	
} //Cierre clase