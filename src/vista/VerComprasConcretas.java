package vista; 
 
import java.awt.event.*; 
import java.beans.PropertyChangeEvent; 
import java.beans.PropertyChangeListener; 
import java.io.FileNotFoundException; 
import java.text.SimpleDateFormat; 
import java.util.ArrayList; 

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal; 

import com.itextpdf.text.Document; 
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.toedter.calendar.JDateChooser; 

import modelo.*;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilPDF;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;
import utilidades.ValidacionesComun; 

public class VerComprasConcretas extends JFrame implements ActionListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos de la clase 
        private JPanel contentPane;
        private JLabel lblFechaInicio;
        private JLabel lblFechaFin;
        private JLabel lblTienda;
        private JLabel lblFormaCompra;
        private JLabel lblCosteTotal; 
        private JLabel lblCantidad;
        
        private JDateChooser dateInicio; 
        private JDateChooser dateFin;
        private JComboBox<String> cbOpciones;
        private JComboBox<String> cbFormaCompra;
        private JComboBox<String> cbTiendas; 
        
        // botones
        private JButton btnVer; 
        private JButton btnMenu; 
        private JButton btnPDF; 

        private Compra compra; 
        private ArrayList<Compra> arrayPDF=new ArrayList<Compra>(); 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private Object [][] arrayTabla; //array bidimensional
        private JTable tabla; 
		private ArrayList<String> listaComprasFichero = FicherosRegistros.obtenerListaComprasFichero(Constantes.FICHERO_COMPRAS);
        
        // Atributos del resultados user 
        private String resultOpcion; 
        private String resultFechaInicio; 
        private String resultFechaFin; 
        private String resultTienda;
        private String resultFormaCompra;
        
        public VerComprasConcretas(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 707, 608); // size 
            setTitle(Constantes.VISTA_COMPRAS_CONCRETAS); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
       		if(!UtilesValida.esNula(listaComprasFichero)) {
    			
       			arrayTabla = Utilidades.obtenerArrayTabla(listaComprasFichero);
    		}
            
            // Carga los componentes de la ventana 
            componentes(); 

            // LISTA DE TIENDAS 
            if(UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTiendas) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            
    		// LISTA FORMA COMPRA		
    		if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_COMPRA), cbFormaCompra) ))
    			JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);
                
        } // Constructor 
        
		private void componentes(){ 
                
            // Layout 
    		contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
    		setContentPane(contentPane);
            
            // Panel para visualizar y hacer scroll 
            scrollPane = Utilidades.obtenerScroll(24, 153, 622, 303, scrollPane, false); 
            contentPane.add(scrollPane);
            
            // Tabla con checkbox
            TableCheckBox tablaCheck = new TableCheckBox();
            modelo = tablaCheck.obtenerModelo(modelo, arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS_SELECT);
            tabla = tablaCheck.obtenerTabla(tabla, modelo, scrollPane, false);
            tabla.addMouseListener(new MouseAdapter() {
            	
            	public void mouseClicked(MouseEvent evento) {
            		int fila = tabla.rowAtPoint(evento.getPoint());
    		        String check = String.valueOf(modelo.getValueAt(fila, 7));
    		        if(check.equals(Constantes.BOOLEAN_FALSE_STRING)) {
    		        	modelo.setValueAt(true,fila, 7);
    		        }
    		        else if(check.equals(Constantes.BOOLEAN_TRUE_STRING)) {
    		        	modelo.setValueAt(false,fila, 7);
    		        }
    		        
            	}
            	
            });

            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 552, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 24, 363, 20, Constantes.LIT_INFO_VER)); // Aviso de seleccion 
            
            cbOpciones = new JComboBox<String>(ListaConstantesValidacion.LISTA_OPCIONES);
            cbOpciones.addActionListener((ActionListener)this);
            cbOpciones.setBounds(230, 23, 149, 20);
            contentPane.add(cbOpciones); 
            
            lblFechaInicio = Utilidades.obtenerLabel(37, 76, 95, 14, Constantes.LABEL_FECHA_INICIO+Constantes.ESPACIO+Constantes.PUNTOS, lblFechaInicio, false); // Label fecha inicio
            contentPane.add(lblFechaInicio); 
            
            dateInicio = Utilidades.obtenerFecha(126, 70, 126, 20, dateInicio, false); // fecha inicio          
            dateInicio.getDateEditor().addPropertyChangeListener( new PropertyChangeListener() { 
                @Override 
                public void propertyChange(PropertyChangeEvent evento) { 
                    if (evento.getPropertyName().equals(Constantes.INPUT_FECHA)) { 

                        resultFechaInicio = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateInicio.getDate()); 
                    } 
                } 
            }); 
            contentPane.add(dateInicio); 
                
            lblFechaFin = Utilidades.obtenerLabel(37, 111, 95, 14, Constantes.LABEL_FECHA_FIN+Constantes.ESPACIO+Constantes.PUNTOS, lblFechaFin, false); // Label fecha fin 
            contentPane.add(lblFechaFin); 
            
            dateFin = Utilidades.obtenerFecha(126, 111, 126, 20, dateFin, false); // fecha fin 
            dateFin.getDateEditor().addPropertyChangeListener( new PropertyChangeListener() { 
                @Override 
                public void propertyChange(PropertyChangeEvent evento) { 
                    if (evento.getPropertyName().equals(Constantes.INPUT_FECHA)) { 

                        resultFechaFin = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateFin.getDate()); 
                    } 
                } 
            }); 
            contentPane.add(dateFin); 
            
            lblFormaCompra = Utilidades.obtenerLabel(400, 73, 90, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS, lblFormaCompra, false); // Label forma compra
            contentPane.add(lblFormaCompra); 
            
            cbFormaCompra = Utilidades.obtenerCombo(495, 70, 149, 20, cbFormaCompra, false); // combo forma compra 
            cbFormaCompra.addActionListener((ActionListener)this); 
            contentPane.add(cbFormaCompra); 
                
            lblTienda = Utilidades.obtenerLabel(400, 114, 95, 14, Constantes.LABEL_TIENDA+Constantes.ESPACIO+Constantes.PUNTOS, lblTienda, false); // Label tienda
            contentPane.add(lblTienda);  
            
            cbTiendas = Utilidades.obtenerCombo(495, 111, 151, 20, cbTiendas, false); // combo tiendas 
            cbTiendas.addActionListener((ActionListener)this); 
            contentPane.add(cbTiendas); 

            lblCosteTotal = Utilidades.obtenerLabel(474, 481, 50, 14, Constantes.LABEL_TOTAL+Constantes.ESPACIO+Constantes.PUNTOS, lblCosteTotal, false); // label total 
            lblCosteTotal.setVisible(false); 
            contentPane.add(lblCosteTotal); 

            lblCantidad = Utilidades.obtenerLabel(522, 481, 78, 14, String.valueOf(""), lblCantidad, false); // label importe 
            lblCantidad.setVisible(false); 
            contentPane.add(lblCantidad); 
            
            btnVer = Utilidades.obtenerBoton(273, 70, 110, 61, Constantes.BTN_VER, btnVer); // boton ver 
            btnVer.addActionListener((ActionListener)this); 
            btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnVer); 
            
            btnMenu = Utilidades.obtenerBoton(237, 527, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
            
            btnPDF = Utilidades.obtenerBoton(369, 527, 95, 41, Constantes.BTN_CREAR, btnPDF); // boton crear pdf 
            btnPDF.addActionListener((ActionListener)this); 
            btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_PDF+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnPDF);
                
        } // Cierre componentes 
        
        @SuppressWarnings("unused") 
        public void actionPerformed(ActionEvent evento){ 

            if(evento.getSource()==cbOpciones){ // si selecciona la lista de opciones 
                    
                if(cbOpciones.isValid()) 
                    resultOpcion=cbOpciones.getSelectedItem().toString(); 
                
                if(!UtilesValida.esNulo(resultOpcion)){ 
                    
                    UtilidadesTabla.limpiarTabla(modelo); 

                    limpiarComponentes(); 

                    if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[1])) 
                    				//    lblF_ini 	F_ini 		lblFin 		F_fin  		lblFormaCompra	 ListFormaCompra 	lblTienda 	ListTienda 		Ver
                        cambiarVisibilidad(true, 	true, 		false, 		false, 		false, 				false, 			false, 		false, 			true);

                    else if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[2])) 
                        cambiarVisibilidad(true,	 true, 		true, 		true,		false, 				false,	 		false, 		false, 			true); 

                    else if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[3])) 
                        cambiarVisibilidad(false, 	false, 		false, 		false, 		true, 				true, 			true, 		true, 			true); 
                }
                else {
                		cambiarVisibilidad(false, 	false, 		false, 		false, 		false, 				false, 			false, 		false, 			true);
                }
            } 

            if(evento.getSource()==cbTiendas){ 
                
                if(cbTiendas.isValid()) 
                        resultTienda=cbTiendas.getSelectedItem().toString(); 
            }
            
            if(evento.getSource()==cbFormaCompra){ 
                
                if(cbFormaCompra.isValid()) 
                        resultFormaCompra=cbFormaCompra.getSelectedItem().toString(); 
            }

                //si ha pulsado el boton "Ver" 
            if(evento.getSource()==btnVer){
                
                if(!UtilesValida.esNulo(resultOpcion)){
                	
                	UtilidadesTabla.limpiarTabla(modelo);
	
	                if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[1])){ // 1 Fecha 
	                	
                        boolean validado = false;
                        
                        validado = ValidacionesComun.validarCamposComprasConcretasSoloFecha(resultFechaInicio);

                        if(validado) {
                        	
                        	if(!UtilesValida.esNula(listaComprasFichero)) {
                        		
                        		arrayTabla = Utilidades.obtenerArrayTabla(listaComprasFichero, resultFechaInicio, resultFechaFin, resultTienda, resultFormaCompra, resultOpcion);
                        		modelo.setDataVector(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS_SELECT);
                        		
	                        	if(arrayTabla.length == Constantes.DEFAULT_INT_CERO)
	                        		JOptionPane.showMessageDialog(null, Constantes.LIT_INFO_TABLA_REGISTROS, Constantes.VALIDA_TABLA_REGISTROS, JOptionPane.INFORMATION_MESSAGE);
                        		
                        	}
                        }

	                } 
	                else if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[2])){ // Entre fechas 
	                	
                        boolean validado = false;
                        
                        validado = ValidacionesComun.validarCamposComprasConcretasEntreFechas(resultFechaInicio, resultFechaFin);

                        if(validado) {
                        	
                        	if(!UtilesValida.esNula(listaComprasFichero)) {
                        		
                        		arrayTabla = Utilidades.obtenerArrayTabla(listaComprasFichero, resultFechaInicio, resultFechaFin, resultTienda, resultFormaCompra, resultOpcion);
                        		modelo.setDataVector(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS_SELECT);
                        		
	                        	if(arrayTabla.length == Constantes.DEFAULT_INT_CERO)
	                        		JOptionPane.showMessageDialog(null, Constantes.LIT_INFO_TABLA_REGISTROS, Constantes.VALIDA_TABLA_REGISTROS, JOptionPane.INFORMATION_MESSAGE);
                        	}
                        	
                        }
	                   
	                } 
	                if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[3])){ // Tiendas 
	
                        boolean validado = false;
                        
                        validado = ValidacionesComun.validarCamposComprasConcretasSoloTienda(resultTienda);

                        if(validado) {
                        	
                        	if(!UtilesValida.esNula(listaComprasFichero)) {
                        		
                        		arrayTabla = Utilidades.obtenerArrayTabla(listaComprasFichero, resultFechaInicio, resultFechaFin, resultTienda, resultFormaCompra, resultOpcion);
                        		modelo.setDataVector(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS_SELECT);
                        		
	                        	if(arrayTabla.length == Constantes.DEFAULT_INT_CERO)
	                        		JOptionPane.showMessageDialog(null, Constantes.LIT_INFO_TABLA_REGISTROS, Constantes.VALIDA_TABLA_REGISTROS, JOptionPane.INFORMATION_MESSAGE);
                        	}
                        }
	                } 

                }
                else {
                	
                	JOptionPane.showMessageDialog(null, Constantes.LIT_INFO_VER, Constantes.VALIDA_OPCION, JOptionPane.INFORMATION_MESSAGE);
                }
	                
            } // btnVer 
            
            if(evento.getSource()==btnMenu){ // se guardan los datos y vuelve a la principal 
        
                  ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                  setVisible(false); 
            } 
                
            if(evento.getSource()==btnPDF){ // creara un documento de pdf 

                for (int i = 0; i < tabla.getRowCount(); i++) { 
                        
                    // Se obtienen los resultados de la tabla 
                    String id = tabla.getValueAt(i, 0).toString();
                    String fecha=tabla.getValueAt(i, 1).toString();   
                    String tienda=tabla.getValueAt(i, 2).toString(); 
                    String producto=tabla.getValueAt(i, 3).toString(); 
                    String importe = tabla.getValueAt(i, 4).toString();
                    String formaCompra=tabla.getValueAt(i, 5).toString(); 
                    String pago=tabla.getValueAt(i, 6).toString();
                    Boolean checked = Boolean.valueOf(tabla.getValueAt(i, 7).toString()); 
                    // Si esta seleccionado esa fila, se agrega al array 
                    if (checked) { 
                            // Se crea un objeto Compra 
                            compra = new Compra(Integer.valueOf(id), fecha, tienda, producto, Double.valueOf(importe), formaCompra, pago); 
                            // Se agrega al arraylist 
                            arrayPDF.add(compra); 
                    } 
                } 
                
                // Si la opcion es con 1 fecha, guardara la fecha tambien 
                if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[1])){ // 1 Fecha 
 
                    try {                                         

                        String nombreFichero = Constantes.FICHERO_COMPRAS+Constantes.GUION_BAJO+resultFechaInicio;
                        String titulo        = Constantes.TITULO_PDF_COMPRAS_CONCRETAS+Constantes.ESPACIO+resultFechaInicio;
                            
                        if(crearPDF(nombreFichero, titulo, arrayPDF)) 
                            JOptionPane.showMessageDialog(null, Constantes.MSG_DOC_CREADO_PDF); 
                            
                    } 
                    catch (FileNotFoundException fnfe) { 
                        JOptionPane.showMessageDialog(null, fnfe.getMessage()); 
                    } 
                    catch (DocumentException de) { 
                        JOptionPane.showMessageDialog(null, de.getMessage()); 
                    } 
                    
                } // cierre if 1 fecha 
                
                else if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[2])){ // Entre fechas 
                	
                    try {                 

                        String nombreFichero = Constantes.FICHERO_COMPRAS+Constantes.GUION_BAJO+resultFechaInicio+Constantes.GUION+resultFechaFin; 
                        String titulo        = Constantes.LIT_DESDE+Constantes.ESPACIO+resultFechaInicio+Constantes.ESPACIO+Constantes.LIT_HASTA+Constantes.ESPACIO+resultFechaFin; 
                        
                        if(crearPDF(nombreFichero, titulo, arrayPDF)){ 
        
                            JOptionPane.showMessageDialog(null, Constantes.MSG_DOC_CREADO_PDF); 
                        } 
                        
                    } 
                    catch (FileNotFoundException fnfe) { 
                        JOptionPane.showMessageDialog(null, fnfe.getMessage()); 
                    } 
                    catch (DocumentException de) {
                        JOptionPane.showMessageDialog(null, de.getMessage()); 
                    } 
                    
                } // cierre if Entre fechas 
                
                else if(resultOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[3])){ // Tienda 
                    try {    
                    	
                    	String nombreFichero 	= Constantes.VACIO;
                    	String titulo 			= Constantes.VACIO;

                    	if(UtilesValida.esNulo(resultFormaCompra)) {
                    		
                            nombreFichero = Constantes.FICHERO_COMPRAS+Constantes.GUION_BAJO+resultTienda; 
                            titulo        = Constantes.TITULO_PDF_COMPRAS_CONCRETAS+Constantes.ESPACIO+Constantes.LIT_DE+Constantes.ESPACIO+resultTienda; 
                    	}
                    	else if(!UtilesValida.esNulo(resultFormaCompra)) {
                    	     nombreFichero = Constantes.FICHERO_COMPRAS+Constantes.GUION_BAJO+resultTienda+Constantes.GUION_BAJO+resultFormaCompra; 
                             titulo        = Constantes.TITULO_PDF_COMPRAS_CONCRETAS+Constantes.ESPACIO+Constantes.LIT_DE+Constantes.ESPACIO+resultTienda+Constantes.ESPACIO+Constantes.LIT_DE_FORMA+Constantes.ESPACIO+resultFormaCompra; 
                    	}

                        
                        if(crearPDF(nombreFichero, titulo, arrayPDF)) 
                            JOptionPane.showMessageDialog(null, Constantes.MSG_DOC_CREADO_PDF); 
                            
                    } 
                    catch (FileNotFoundException fnfe) { 
                            JOptionPane.showMessageDialog(null, fnfe.getMessage()); 
                    } 
                    catch (DocumentException de) { 
                            JOptionPane.showMessageDialog(null, de.getMessage()); 
                    } 
                } // cierre tienda 
            } 
        } // Cierre actionPerformed 

        private void limpiarComponentes(){ 
                        
            resultFechaInicio	= Constantes.VACIO; 
            resultFechaFin  	= Constantes.VACIO; 
            resultTienda       	= Constantes.VACIO; 
            lblCosteTotal.setVisible(false); 
            lblCantidad.setVisible(false); 
            lblCantidad.setText(Constantes.VACIO); 
            try{ dateInicio.setCalendar(null); } catch(NullPointerException npe){} 

        } // limpiarComponentes 

        private void cambiarVisibilidad(boolean bFecha, boolean bDateInicio, boolean bFechaFin, boolean bDateFin, boolean bFormaCompra, boolean bListFormaCompra, boolean bTienda, 
        		boolean bListTienda, boolean bVer){ 

            this.lblFechaInicio.setVisible(bFecha); 
            this.dateInicio.setVisible(bDateInicio); 
            this.lblFechaFin.setVisible(bFechaFin); 
            this.dateFin.setVisible(bDateFin); 
            this.lblFormaCompra.setVisible(bFormaCompra);
            this.cbFormaCompra.setVisible(bListFormaCompra); 
            this.lblTienda.setVisible(bTienda); 
            this.cbTiendas.setVisible(bListTienda); 
            this.btnVer.setVisible(bVer); 

        } // cambiarVisibilidad 

       private boolean crearPDF(String nombreFichero, String titulo, ArrayList<Compra> arrayPDF) throws FileNotFoundException, DocumentException { 
        
            Document documento = UtilPDF.crearFicheroPDF(nombreFichero, PageSize.A4.rotate()); 
            boolean pdfCreado = false; 

            if(!UtilesValida.esNulo(documento)){ 

                documento.open(); // se abre el documento a escribir 

                UtilPDF.agregarTitulo(documento, titulo); 

                UtilPDF.agregarTablaContenidoCompra(documento, ListaConstantesValidacion.CABECERA_COMPRAS, arrayPDF, Constantes.NUM_COLUMNAS_COMPRAS); 

                documento.close(); // se cierra el documento 

                pdfCreado = true; 
            } 

            return pdfCreado; 

        } // crearPDF

} // Cierre de clase Concretos


