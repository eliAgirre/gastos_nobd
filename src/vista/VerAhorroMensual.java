package vista; 
 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.beans.PropertyChangeEvent; 
import java.beans.PropertyChangeListener; 
import java.io.FileNotFoundException; 
import java.text.SimpleDateFormat; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.table.DefaultTableModel; 

import com.itextpdf.text.Document; 
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.toedter.calendar.JDateChooser; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilPDF;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;  


public class VerAhorroMensual extends JFrame implements ActionListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private JDateChooser dateInicio; 
        private JDateChooser dateFin; 
        private JButton btnGuardar; 
        private JButton btnCalcular; 
        private JButton btnConsultarM; 
        private JButton btnMenu; 
        private JButton btnPDF;
                
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        //private static Object[] datos=new Object[5]; 
        private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_AHORRO]; 
        private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_PRESUPUESTOS); 
        
        // Atributos del resultados user 
        private String resultFechaInicio; 
        private String resultFechaFin;
        
        public VerAhorroMensual(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(0, -29, 554, 598); // size 
            setTitle(Constantes.VISTA_AHORRO_MENSUAL); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la ventana 
            componentes(); 
            
            modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                
        } // constructor 
        
        public void componentes(){ 
            
            // Layout 
	        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
	        setContentPane(contentPane); 
            
            // Panel para visualizar y hacer scroll 
            scrollPane = Utilidades.obtenerScroll(61, 199, 462, 303, scrollPane, false); 
            contentPane.add(scrollPane); 
            
            // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
            modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_AHORRO); 
            
            // Se le pasa a JTable el modelo de tabla 
            tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
            tabla.setDefaultRenderer(Object.class, new MiRender()); // Para editar el color del texto de la tabla 
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 544, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_VER_AHORRO_MENSUAL)); // Aviso de visualizacion 
            
            contentPane.add(Utilidades.obtenerLabel(61, 82, 111, 20, Constantes.LABEL_FECHA_INICIO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha inicio 
            
            dateInicio = Utilidades.obtenerFecha(159, 82, 126, 20, dateInicio); // fecha inicio 
            dateInicio.getDateEditor().addPropertyChangeListener( new PropertyChangeListener() { 
                @Override 
                public void propertyChange(PropertyChangeEvent evento) { 
                    if (evento.getPropertyName().equals(Constantes.INPUT_FECHA_INICIO)) { 

                        resultFechaInicio = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateInicio.getDate()); 
                    } 
                } 
            }); 
            contentPane.add(dateInicio); 

            contentPane.add(Utilidades.obtenerLabel(61, 119, 111, 20, Constantes.LABEL_FECHA_FIN+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha fin 
            
            dateFin = Utilidades.obtenerFecha(159, 119, 126, 20, dateFin); // fecha fin 
            dateFin.getDateEditor().addPropertyChangeListener( new PropertyChangeListener() { 
                @Override 
                public void propertyChange(PropertyChangeEvent evento) { 
                    if (evento.getPropertyName().equals(Constantes.INPUT_FECHA_FIN)) { 

                        resultFechaFin = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateFin.getDate()); 
                    } 
                } 
            }); 
            contentPane.add(dateFin); 
            
            btnGuardar = Utilidades.obtenerBoton(325, 49, 137, 33, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar 
            btnGuardar.addActionListener((ActionListener)this); 
            btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnGuardar); 
            
            btnCalcular = Utilidades.obtenerBoton(325, 93, 137, 33, Constantes.BTN_CALCULAR, btnCalcular); // boton calcular 
            btnCalcular.addActionListener((ActionListener)this); 
            btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_CALC+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnCalcular); 
            
            btnConsultarM = Utilidades.obtenerBoton(325, 137, 137, 33, Constantes.BTN_CONSULTAR, btnConsultarM); // boton consultar 
            btnConsultarM.addActionListener((ActionListener)this); 
            btnConsultarM.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnConsultarM); 
            
            btnMenu = Utilidades.obtenerBoton(159, 525, 126, 33, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
            
            btnPDF = Utilidades.obtenerBoton(325, 525, 137, 33, Constantes.BTN_CREAR, btnPDF); // boton pdf 
            btnPDF.addActionListener((ActionListener)this); 
            btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_PDF+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnPDF); 
                
        } // Cierre componentes 
        
        @SuppressWarnings("unused") 
        public void actionPerformed(ActionEvent evento) { 
                
                if(evento.getSource()==btnGuardar){ 
                        
                    UtilidadesTabla.limpiarTabla(modelo); 

                    
                    limpiarComponentes(); 
                } 
                
                // Si hace clic en "Calcular", se obtiene un numero de ahorro 
                if(evento.getSource()==btnCalcular){
                	
                	UtilidadesTabla.limpiarTabla(modelo); 

                    
                    limpiarComponentes(); 
                	
                } 
                // Si hace clic en "Consultar", consulta los ahorros que estan en la BD 
                if(evento.getSource()==btnConsultarM){
                	
                	UtilidadesTabla.limpiarTabla(modelo); 

                    
                    limpiarComponentes();
                	
                } 
                // Si hace clic en Menu vuelve a la principal 
                if(evento.getSource()==btnMenu){ 
                        
                      ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                      setVisible(false); 
                } 
                
                if(evento.getSource()==btnPDF){ 
                    try { 
                            
                        Document documento = UtilPDF.crearFicheroPDF(Constantes.FICHERO_AHORRO_MENSUAL, PageSize.A4.rotate()); 

                        if(!UtilesValida.esNulo(documento)){ 
                                
                            documento.open(); // se abre el documento a escribir 
                            
                            UtilPDF.agregarTitulo(documento, Constantes.TITULO_PDF_AHORRO_MENSUAL); 
                            
                            //UtilPDF.agregarTabla(documento, ListaConstantesValidacion.CABECERA_AHORRO, arrayPDF, Constantes.NUM_COLUMNAS_AHORRO); 
                            
                            documento.close(); // se cierra el documento 
                            
                            JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF"); 
                                
                        }                                 
                    } 
                    catch (FileNotFoundException fnfe) { 
                            JOptionPane.showMessageDialog(null, fnfe.getMessage()); 
                    } 
                    catch (DocumentException de) { 
                            JOptionPane.showMessageDialog(null, de.getMessage()); 
                    } 
                } 
        } // Cierre actionPerformed 
        
        private void limpiarComponentes(){ 
            
            resultFechaInicio = Constantes.VACIO; 
            resultFechaFin = Constantes.VACIO; 
            try{ dateInicio.setCalendar(null); } 
            catch(NullPointerException npe){} 
            
            try{ dateFin.setCalendar(null); } 
            catch(NullPointerException npe){} 
        }
        
} // Cierre clase 
