package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.beans.PropertyChangeEvent; 
import java.beans.PropertyChangeListener; 
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat; 
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

import javax.swing.*; 
import javax.swing.table.DefaultTableModel; 

import modelo.Ahorro;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilPDF;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;

import com.itextpdf.text.Document; 
import com.itextpdf.text.DocumentException; 
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.toedter.calendar.JDateChooser;

import controlador.ControladorPrincipal; 

public class VerAhorroSemanal extends JFrame implements ActionListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private Double presu=0.0; 
        private Double gastos=0.0; 
        private float ahorro=(float) 0.0; 
        private int idAhorroSemanal = Constantes.DEFAULT_INT_CERO;
        private JDateChooser dateInicio; 
        private JDateChooser dateFin; 
        private JButton btnGuardar; 
        private JButton btnCalcular; 
        private JButton btnConsultar;         
        private JButton btnMenu; 
        private JButton btnPDF; 
        private Font avisoFont=new Font(); 
        private Font avisoFechas=new Font(); 
        private Ahorro ahorroModel; 
        private ArrayList<Ahorro> arrayPDF = new ArrayList<Ahorro>(); 
                
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla;
        
        private ArrayList<String> registrosCompras = FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS);
        private ArrayList<String> registrosGastos = FicherosRegistros.leerRegistros(Constantes.FICHERO_GASTOS_FIJOS);
        private ArrayList<String> registrosPresus = FicherosRegistros.leerRegistros(Constantes.FICHERO_PRESUPUESTOS);
        
        private static String[] datosFileCompras = new String[Constantes.NUM_COLUMNAS_COMPRAS];
        private static String[] datosFileGastos = new String[Constantes.NUM_COLUMNAS_GASTOS_FIJOS];
        private static String[] datosFilePresus = new String[Constantes.NUM_COLUMNAS_PRESUS];
        private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_AHORRO];      
        
        // Atributos del resultados user 
        private String resultFechaInicio; 
        private String resultFechaFin; 
        
        public VerAhorroSemanal(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(0, -29, 554, 598); // size 
            setTitle(Constantes.VISTA_AHORRO_SEMANAL); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la ventana 
            componentes();
 			
        } // Cierre del constructor 
        
        public void componentes(){
                
			// Layout 
			contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
			setContentPane(contentPane); 
			    
			// Panel para visualizar y hacer scroll 
			scrollPane = Utilidades.obtenerScroll(61, 199, 462, 303, scrollPane, false); 
			contentPane.add(scrollPane); 
			        
			// Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
			modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_AHORRO); 
			
			// Se le pasa a JTable el modelo de tabla 
			tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
			tabla.setDefaultRenderer(Object.class, new MiRender()); // Para editar el color del texto de la tabla 
			
			// Labels + textFields + comboBox + buttons 
			contentPane.add(Utilidades.obtenerVersionLabel(0, 544, 95, 14)); // Version label 
			
			contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_VER_AHORRO_SEMANAL)); // Aviso de visualizacion 
			
			contentPane.add(Utilidades.obtenerLabel(61, 82, 111, 20, Constantes.LABEL_FECHA_INICIO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha inicio
			
			dateInicio = Utilidades.obtenerFecha(159, 82, 126, 20, dateInicio); // fecha inicio 
			dateInicio.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals(Constantes.INPUT_FECHA)) {
						// Se le da formato a la fecha obtenida
						resultFechaInicio = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateInicio.getDate());
			        }
				}
			});
			contentPane.add(dateInicio);
			
			contentPane.add(Utilidades.obtenerLabel(61, 119, 111, 20, Constantes.LABEL_FECHA_FIN+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha fin 
			
			dateFin = Utilidades.obtenerFecha(159, 119, 126, 20, dateFin); // fecha fin 
			dateFin.getDateEditor().addPropertyChangeListener( new PropertyChangeListener() { 
		        @Override 
		        public void propertyChange(PropertyChangeEvent evento) { 
		        	
	                if (evento.getPropertyName().equals(Constantes.INPUT_FECHA))
	                	resultFechaFin = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO).format(dateFin.getDate()); 
		        } 
			}); 
			contentPane.add(dateFin); 
			
			btnGuardar = Utilidades.obtenerBoton(325, 49, 137, 33, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar 
			btnGuardar.addActionListener((ActionListener)this); 
			btnGuardar.setEnabled(esEnabled());
			btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG))); 
			contentPane.add(btnGuardar); 
			
			btnCalcular = Utilidades.obtenerBoton(325, 93, 137, 33, Constantes.BTN_CALCULAR, btnCalcular); // boton calcular 
			btnCalcular.addActionListener((ActionListener)this); 
			btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_CALC+Constantes.EXTENSION_PNG))); 
			contentPane.add(btnCalcular); 
			
			btnConsultar = Utilidades.obtenerBoton(325, 137, 137, 33, Constantes.BTN_CONSULTAR, btnConsultar); // boton consultar 
			btnConsultar.addActionListener((ActionListener)this); 
			btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
			contentPane.add(btnConsultar); 
			
			btnMenu = Utilidades.obtenerBoton(159, 525, 126, 33, Constantes.BTN_MENU, btnMenu); // boton menu 
			btnMenu.addActionListener((ActionListener)this); 
			btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
			contentPane.add(btnMenu); 
			
			btnPDF = Utilidades.obtenerBoton(325, 525, 137, 33, Constantes.BTN_CREAR, btnPDF); // boton pdf 
			btnPDF.addActionListener((ActionListener)this); 
			btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_PDF+Constantes.EXTENSION_PNG))); 
			contentPane.add(btnPDF); 
                
        } // Cierre componentes 
        
        @SuppressWarnings("unused") 
        public void actionPerformed(ActionEvent evento) {
            
            if(evento.getSource()==btnGuardar){ 
                    
                UtilidadesTabla.limpiarTabla(modelo); 
                
                if( (!UtilesValida.esNulo(resultFechaInicio) && UtilesValida.esFecha(resultFechaInicio)) && (!UtilesValida.esNulo(resultFechaFin) && UtilesValida.esFecha(resultFechaFin)) ){
                	
                	if(!UtilesValida.esNulo(registrosPresus)) {
                		
                		String ultimaLinea = FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_PRESUPUESTOS);
                		
                		 if(!UtilesValida.isNull(ultimaLinea)) {
                			 
                			 if(!ultimaLinea.equals(Constantes.VACIO)) {
                				 
                				 for(int i=0;i<registrosPresus.size();i++) {
                					 
                					 datosFilePresus = registrosPresus.get(i).split(Constantes.PUNTO_COMA);
                					 
                				 }
                				 
                			 } // ultima linea no es vacia
                			 
                		 } // ultima linea no es nula
                		
                	}
                        
                    /*if(!UtilesValida.esNulo(registros)) { 
                            
                        if(!FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_AHORRO_SEMANAL)) 
                                FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_AHORRO_SEMANAL); 
                        
                        if(!FicherosLog.ficheroLogCreado(Constantes.FICHERO_AHORRO_SEMANAL)) 
                                FicherosLog.ficheroLogCreado(Constantes.FICHERO_AHORRO_SEMANAL); 
                        
                        // Se obtiene la ultima linea 
                        //String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_AHORRO_SEMANAL) 
                        String ultimaLinea = FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_AHORRO_SEMANAL); 
                        
                        if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                        
                        
                            if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia 
                                    
                                //FicherosLog.cabeceraAhorroSemanalLog(); // se inserta la cabecera 
                        
                                idAhorroSemanal++; 
                                
                                for(int i=0;i<registros.size();i++) { 
                                        
                                    datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 

                                    if(datosFile[0].equals(resultFechaInicio) && datosFile[1].equals(resultFechaFin) && datosFile[3].equals(Constantes.RADIO_SEMANAL)) { 
                                        // Se agrega cada fila al modelo de tabla 
                                        modelo.addRow(datosFile);                                                                         
                                        
                                        //Utilidades.agregarAhorroSemanalALista(idAhorroSemanal, resultFechaInicio, resultFechaFin, resultGasto, resultPresu, resultAhorro, listaAhorros); 
                                        
                                        //FicherosRegistros.insertarRegistroAhorroSemanal(String.valueOf(idAhorroSemanal), resultFechaInicio, resultFechaFin, resultGasto, resultPresu, resultAhorro); 
                                    } 
                                } 
                               
                                limpiarComponentes(); 
                                    
                            } // ultima linea es vacia 
                            else { 
                                    
                                    
                                    
                            } // ultima linea no es vacia 
                        } // ultima linea no es nula 
                    } */
                } 
                else if( (!UtilesValida.esNulo(resultFechaInicio) && UtilesValida.esFecha(resultFechaInicio)) && UtilesValida.esNulo(resultFechaFin) ){ 
                        
                    /*if(!UtilesValida.esNulo(registros)) { 
                        
                        for(int i=0;i<registros.size();i++) { 
                                
                            datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                            
                            if(datosFile[0].equals(resultFechaInicio) && datosFile[3].equals(Constantes.RADIO_SEMANAL)) { 
                                // Se agrega cada fila al modelo de tabla 
                                modelo.addRow(datosFile); 
                            } 
                        } 
                    }*/ 
                } 
                else if( UtilesValida.esNulo(resultFechaInicio) && (!UtilesValida.esNulo(resultFechaFin) && UtilesValida.esFecha(resultFechaFin)) ){ 
                        
                    /*if(!UtilesValida.esNulo(registros)) { 
                        
                        for(int i=0;i<registros.size();i++) { 
                                
                            datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                            
                            if(datosFile[1].equals(resultFechaFin) && datosFile[3].equals(Constantes.RADIO_SEMANAL)) { 
                                // Se agrega cada fila al modelo de tabla 
                                modelo.addRow(datosFile); 
                            } 
                        } 
                    }*/
                } 
                
                limpiarComponentes(); 
                    
            } // guardar 
            
            // Si hace clic en "Calcular", se obtiene un numero de ahorro 
            if(evento.getSource()==btnCalcular){ 
                    
                    UtilidadesTabla.limpiarTabla(modelo); 
                    
                    /*try { 
                            // Sentencia SELECT presu 
                            sql = "SELECT cantidad FROM presupuestos WHERE fecha_presu='"+resultFechaInicio+"' and tipo='Semanal';"; 
                            // Hace la consulta y devuelve el resultado                         
                            rs = BD.consulta(stmt,sql); 
                            // Mientras que haya datos 
                            while (rs.next()) { 
                                    // Se guardan los datos en array 
                                    datos[0]=resultFechaInicio; 
                                    datos[1]=resultFechaFin; 
                                    datos[2]=Double.toString(rs.getDouble(1)); 
                                    presu=rs.getDouble(1); 
                            } // Cierre de while 
                            rs.close(); //Cierre de la consulta 
                            
                            // Sentencia SELECT compras 
                            sql = "SELECT SUM(coste) FROM compras WHERE fecha BETWEEN '"+resultFechaInicio+"' AND '"+resultFechaFin+"';"; 
                            // Hace la consulta y devuelve el resultado                         
                            rs = BD.consulta(stmt,sql); 
                            // Mientras que haya datos 
                            while (rs.next()) { 
                                    datos[3]=Double.toString(rs.getDouble(1)); 
                                    gastos=rs.getDouble(1); 
                                    ahorro=(float)(presu-gastos);                                           
                                    datos[4]=ahorro; 
                                    // Se a�ade cada fila al modelo de tabla 
                                    modelo.addRow(datos); 
                            } // Cierre de while 
                            
                            rs.close(); //Cierre de la consulta 
                    } 
                    catch (SQLException e) { 
                            // Muestra error SQL 
                            JOptionPane.showMessageDialog(null, e.getMessage()); 
                            
                    } // Cierre excepcion*/ 
            
                    limpiarComponentes(); 
                    
            } // calcular 

            if(evento.getSource()==btnConsultar){
            	
                UtilidadesTabla.limpiarTabla(modelo); 
                
                if( (!UtilesValida.esNulo(resultFechaInicio) && UtilesValida.esFecha(resultFechaInicio)) && (!UtilesValida.esNulo(resultFechaFin) && UtilesValida.esFecha(resultFechaFin)) ){
                	
                	double totalPresus = Constantes.DEFAULT_DOBLE_CERO;
                	double totalCompras = Constantes.DEFAULT_DOBLE_CERO;
                	double totalFijos = Constantes.DEFAULT_DOBLE_CERO;
                	double totalGastos = Constantes.DEFAULT_DOBLE_CERO;
                	double totalAhorro = Constantes.DEFAULT_DOBLE_CERO;
                	
                	if(!UtilesValida.esNulo(registrosPresus)) {
                		
                		String ultimaLinea = FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_PRESUPUESTOS);
                		
                		 if(!UtilesValida.isNull(ultimaLinea)) {
                			 
                			 if(!ultimaLinea.equals(Constantes.VACIO)) {
                				 
                				 Date fechaPresu = null;
                				 Date fechaInicio = null;
                				 Date fechaFin = null;
                				 
                				 for(int i=0;i<registrosPresus.size();i++) {
                					 
                					 datosFilePresus = registrosPresus.get(i).split(Constantes.PUNTO_COMA);
                					 
                					 if(datosFilePresus[2].equals(Constantes.RADIO_SEMANAL)) {
                					 
	                					 if(UtilesValida.esFecha(datosFilePresus[1])) {
	                						 
	                						 fechaPresu 	= Utilidades.parseaFecha(datosFilePresus[1]);
	                						 fechaInicio 	= Utilidades.parseaFecha(resultFechaInicio);
	                						 fechaFin	 	= Utilidades.parseaFecha(resultFechaFin);
	                					 }
	                					 
	                					 if(UtilesValida.betweenFechas(fechaPresu, fechaInicio, fechaFin))
	                						 totalPresus = totalPresus + Double.parseDouble(datosFilePresus[3]);
                					 }
                				 }
                				 
                				 
                			 } // ultima linea no es vacia
                			 
                		 } // ultima linea no es nula
                		
                	} // si hay presupuestos
                	
                	if(!UtilesValida.esNulo(registrosCompras)) {
                		
                		String ultimaLinea = FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_COMPRAS);
                		
	               		 if(!UtilesValida.isNull(ultimaLinea)) {
	               			 
	               			 if(!ultimaLinea.equals(Constantes.VACIO)) {
	               				 
	               				 Date fechaCompra = null;
	               				 Date fechaInicio = null;
	               				 Date fechaFin = null;
	               				 
	               				 for(int i=0;i<registrosCompras.size();i++) {
	               					 
	               					 datosFileCompras = registrosCompras.get(i).split(Constantes.PUNTO_COMA);
	               					
                					 if(UtilesValida.esFecha(datosFileCompras[1])) {
                						 
                						 fechaCompra 	= Utilidades.parseaFecha(datosFileCompras[1]);
                						 fechaInicio 	= Utilidades.parseaFecha(resultFechaInicio);
                						 fechaFin	 	= Utilidades.parseaFecha(resultFechaFin);
                					 }
                					 
                					 if(UtilesValida.betweenFechas(fechaCompra, fechaInicio, fechaFin))
                						 totalCompras = totalCompras + Double.parseDouble(datosFileCompras[3]);
	               					 
	               				 }
	               				 
	               				 
	               			 } // ultima linea no es vacia
	               			 
	               		 } // ultima linea no es nula
                	} // si hay compras
                	
                	
                } 
                else if(UtilesValida.esNulo(resultFechaInicio))
                	JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.LABEL_FECHA_INICIO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
                	
                else if(UtilesValida.esNulo(resultFechaFin))
                	JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.LABEL_FECHA_FIN+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
            	
            } // consultar 

            if(evento.getSource()==btnMenu){ 

                  ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                  setVisible(false); 
            } 
                
            // se crea pdf de ahorro semanal 
            if(evento.getSource()==btnPDF){ 
                    
                try { 
                        
                    Document documento = UtilPDF.crearFicheroPDF(Constantes.FICHERO_AHORRO_SEMANAL, PageSize.A4.rotate()); 

                    if(!UtilesValida.esNulo(documento)){ 
                            
                        documento.open(); // se abre el documento a escribir 
                        
                        UtilPDF.agregarTitulo(documento, Constantes.TITULO_PDF_AHORRO_SEMANAL); 
                        
                        UtilPDF.agregarTablaContenidoAhorro(documento, ListaConstantesValidacion.CABECERA_AHORRO, arrayPDF, Constantes.NUM_COLUMNAS_AHORRO); 
                        
                        documento.close(); // se cierra el documento 
                        
                        JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF"); 
                            
                    }                                 
                } 
                catch (FileNotFoundException fnfe) { 
                        JOptionPane.showMessageDialog(null, fnfe.getMessage()); 
                } 
                catch (DocumentException de) { 
                        JOptionPane.showMessageDialog(null, de.getMessage()); 
                } 
            } // crear pdf 
            
        } // Cierre actionPerformed 
        
        private void limpiarComponentes(){ 
            
            resultFechaInicio = Constantes.VACIO; 
            resultFechaFin = Constantes.VACIO; 
            try{ dateInicio.setCalendar(null); } 
            catch(NullPointerException npe){} 
            
            try{ dateFin.setCalendar(null); } 
            catch(NullPointerException npe){} 
        } 
        
        private boolean esEnabled() {
        	
        	boolean enabled = false;

 			Calendar calendario = Calendar.getInstance();

 			int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);
 			
 			if(diaSemana==1) // Si el dia de la semana es 1 (domingo)
 				enabled = true;
 			
 			return enabled;
        }
        
} // Cierre de la clase