package vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import utilidades.UtilidadesTabla;

public class TableCheckBox extends JFrame {

    private static final long serialVersionUID = 1L;

    public TableCheckBox() {}
    
    public DefaultTableModel obtenerModelo(DefaultTableModel modelo, Object[][] datos, String[] cabecera) {
    	
    	modelo =  new DefaultTableModel(datos, cabecera);
    	
    	return modelo;
    }
    
    public JTable obtenerTabla(JTable tabla,  DefaultTableModel modelo, JScrollPane scrollPane, boolean enabled) {
    	
        tabla = new JTable(modelo) {

            private static final long serialVersionUID = 1L;

            @Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                    	 return String.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                    case 3:
                        return String.class;
                    case 5:
                        return String.class;
                    case 6:
                        return String.class;
                    case 7:
                        return Boolean.class;
                    default:
                        return String.class;
                }
            }
        };
    	UtilidadesTabla.ocultaColumnaID(tabla);
        tabla.setEnabled(enabled);
        tabla.setBorder(null);
        scrollPane.setViewportView(tabla);
    	return tabla;
    }
}
