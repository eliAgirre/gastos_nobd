package utilidades;

public class ListaConstantesValidacion {
	
	/**
	 * Es el tipo de comunicacion. [0] ALTA, [1] EDITAR, [2] BAJA.
	 */
	public static final String[] TIPO_COMUNICACION 		= { "ALTA", "EDIT", "BAJA" };
	
	public static final String[] CABECERA_COMPRAS 		= {"ID", "Fecha", "Tienda", "Producto", "Importe", "Forma Compra", "Pago"};
    
    public static final String[] CABECERA_GASTOS_FIJOS	= {"ID", "Fecha doc", "Num doc",  "F. inicio", "F. Fin", "Gasto", "Duracion", "Importe"}; 
    
    public static final String[] CABECERA_INGRESOS      = {"ID", "Fecha", "Concepto", "Importe"};
    
    public static final String[] CABECERA_PRESUPUESTOS  = {"ID", "Fecha", "Tipo", "Importe"};
    
    public static final String[] CABECERA_AHORRO  		= {"ID",  "F. inicio", "F. Fin", "Presu", "Gasto", "Ahorro"}; 

    public static final String[] LISTA_OPCIONES         = {"", "1 Fecha", "Entre fechas", "Tiendas"};
    
    public static final String[] CABECERA_COMPRAS_SELECT= {"ID", "Fecha", "Tienda", "Producto", "Importe", "Forma Compra", "Pago", "Seleccionar"};
    
    public static final String[] CABECERA_TIENDAS       = {"ID", "Tienda"}; 
    
    public static final String[] CABECERA_FORMAS_COMPRA = {"ID", "Forma compra"}; 
    
    public static final String[] CABECERA_FORMAS_PAGO   = {"ID", "Forma pago"};

}
