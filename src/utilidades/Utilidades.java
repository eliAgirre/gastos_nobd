package utilidades;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.json.JSONException;

import com.toedter.calendar.JDateChooser;

import gestor.Compras;
import gestor.JSON;
import modelo.Compra;

public class Utilidades {
	
	/**
	 * Obtiene la hora y fecha del sistema
	 * @return ts Devuelve un objeto de tipo Timestamp.
	 */
 	public static Timestamp obtenerHoraSistema(){
 		
 		// Se crea el objeto calendar y obtiene la hora del sistema
 		Calendar cal = Calendar.getInstance();
 		// Se convierte el objeto calendar a timestamp
 		Timestamp ts = new Timestamp(cal.getTimeInMillis()); 
 		// Devuelve fecha y hora en Timestamp 
 		return ts;
 		
 	} //Cierre obtenerHoraSistema
 	
	/**
	 * Obtiene la version de la aplicacion.
	 * @return lblVersion Devuelve una version de tipo JLabel.
	 */
	public static JLabel obtenerVersionLabel(int x, int y, int width, int height) {
		
		JLabel lblVersion = new JLabel(Constantes.LIT_VERSION+Constantes.PUNTOS+Constantes.ESPACIO+Constantes.VERSION);
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(x, y, width, height);
		return lblVersion;
	}
    
    /** Obtiene el label del aviso de informacion. 
    * @return lblAviso Devuelve una informacion de tipo JLabel. 
    */ 
    public static JLabel obtenerAvisoInfoLabel(int x, int y, int width, int height, String literal) { 

        JLabel lblAviso = new JLabel(literal); 
        lblAviso.setFont(new Font(Constantes.FUENTE_ARIAL, Font.BOLD, 12)); 
        lblAviso.setHorizontalAlignment(SwingConstants.LEFT); 
        lblAviso.setBounds(x, y, width, height); 
        return lblAviso; 
    } 
    
    /** Obtiene el label. 
    * @return label Devuelve el label. 
    */ 
    public static JLabel obtenerLabel(int x, int y, int width, int height, String literal) { 

        JLabel label = new JLabel(literal); 
        label.setForeground(new Color(0, 139, 139)); 
        label.setFont(new Font(Constantes.FUENTE_TAHOMA, Font.BOLD, 11)); 
        label.setBounds(x, y, width, height); 
        return label; 
    }
    
    /** Obtiene el label. 
    * @return label Devuelve el label. 
    */ 
    public static JLabel obtenerLabel(int x, int y, int width, int height, String literal, JLabel label, boolean visible) { 

        label = new JLabel(literal); 
        label.setForeground(new Color(0, 139, 139)); 
        label.setFont(new Font(Constantes.FUENTE_TAHOMA, Font.BOLD, 11)); 
        label.setBounds(x, y, width, height); 
        label.setVisible(visible);
        return label; 
    }

    /** Obtiene el boton. 
    * @return boton Devuelve el boton de tipo JButton. 
    */ 
    public static JButton obtenerBoton(int x, int y, int width, int height, String nombreBoton, JButton boton) { 
            
        boton = new JButton(nombreBoton);  
        boton.setBackground(new Color(184, 231, 255)); 
        boton.setBounds(x, y, width, height);
        return boton; 
    }
    
    /** Obtiene el combo. 
    * @return combo Devuelve el combo de tipo JComboBox. 
    */ 
    public static JComboBox<String> obtenerCombo(int x, int y, int width, int height, JComboBox<String> combo) { 
            
    	combo = new JComboBox<String>();  
    	//combo.setBackground(new Color(184, 231, 255)); 
    	combo.setBounds(x, y, width, height); 
        return combo; 
    }
    
    /** Obtiene el combo. 
    * @return combo Devuelve el combo de tipo JComboBox. 
    */ 
    public static JComboBox<String> obtenerCombo(int x, int y, int width, int height, JComboBox<String> combo, boolean visible) { 
            
    	combo = new JComboBox<String>();  
    	//combo.setBackground(new Color(184, 231, 255)); 
    	combo.setBounds(x, y, width, height); 
    	combo.setVisible(visible);
        return combo; 
    }
    
    /** Obtiene el input de text. 
    * @return txtField Devuelve el input de tipo txtField. 
    */ 
    public static JTextField obtenerTextField(int x, int y, int width, int height, int columns, JTextField txtField, String texto, boolean editable) { 
            
    	txtField = new JTextField();  
    	txtField.setColumns(columns);
    	if(!UtilesValida.esNulo(texto)) { txtField.setText(texto); }
    	txtField.setEditable(editable);
    	txtField.setBounds(x, y, width, height); 
        return txtField; 
    }
    
    /** Obtiene el input de fecha. 
    * @return dateChooser Devuelve el input de fecha de tipo JDateChooser. 
    */ 
   public static JDateChooser obtenerFecha(int x, int y, int width, int height, JDateChooser dateChooser) {
            
        dateChooser = new JDateChooser(); 
        dateChooser.setDateFormatString(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO); 
        dateChooser.setBounds(x, y, width, height);
        return dateChooser; 
   }
   
   /** Obtiene el input de fecha. 
   * @return dateChooser Devuelve el input de fecha de tipo JDateChooser. 
   */ 
  public static JDateChooser obtenerFecha(int x, int y, int width, int height, JDateChooser dateChooser, boolean visible) {
           
       dateChooser = new JDateChooser(); 
       dateChooser.setDateFormatString(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO); 
       dateChooser.setBounds(x, y, width, height);
       dateChooser.setVisible(visible);
       return dateChooser; 
  }

   /** Obtiene la radio. 
   * @return radio Devuelve un radio de tipo JRadioButton. 
   */ 
   public static JRadioButton obtenerRadio(int x, int y, int width, int height, String nombreRadio, boolean selected, ButtonGroup buttonGroup, JRadioButton radio) { 
           
       radio = new JRadioButton(nombreRadio); 
       radio.setSelected(selected); 
       buttonGroup.add(radio); 
       radio.setBounds(x, y, width, height); 
       return radio; 
   } 

    
    /** Obtiene el panel de scroll a mostrar. 
    * @return scrollPane Devuelve el boton de todos de tipo JButton. 
    */ 
    public static JScrollPane obtenerScroll(int x, int y, int width, int height, JScrollPane scrollPane, boolean enabled) { 
            
        scrollPane = new JScrollPane(); 
        scrollPane.setViewport(null); 
        scrollPane.setViewportBorder(null);
        scrollPane.setEnabled(enabled); 
        scrollPane.setBounds(x, y, width, height); 
        return scrollPane; 
    }
    
    /** Obtiene la tabla a mostrar.
    * @return tabla Devuelve la tabla de tipo JTable. 
    */
    public static JTable obtenerTabla(JTable tabla, DefaultTableModel modelo, boolean enabled, JScrollPane scrollPane) {
    	
    	tabla = new JTable(modelo);
    	UtilidadesTabla.ocultaColumnaID(tabla);
        tabla.setEnabled(enabled);
        tabla.setBorder(null);
        scrollPane.setViewportView(tabla);
    	return tabla;
    }
    
    /** Obtiene el panel para mostrar los elementos de la ventana.
    * @return contentPane Devuelve el panel de tipo JPanel. 
    */
    public static JPanel obtenerPanel(int x, int y, int width, int height, JPanel contentPane) {
    	
    	contentPane = new JPanel();
    	contentPane.setBorder(new EmptyBorder(x, y, width, height)); 
        contentPane.setLayout(null);
    	return contentPane;
    	
    }
    
    public static JTextArea obtenerArea(JTextArea textArea, boolean editable, JScrollPane scrollPane) {
    	
    	textArea = new JTextArea();
    	textArea.setEditable(editable);
    	scrollPane.setViewportView(textArea);
    	textArea.setFont(new java.awt.Font(null, 0, 12));
    	return textArea;
    }
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idCompra Es el identificador de la compra.
	 * @param fechaCompra Fecha de la compra.
	 * @param tipoCompra Es el tipo de compra que se ha realizado, como comida, pescaderia, famarcia, etc.
	 * @param tienda Tienda efectuado la compra.
	 * @param importe El importe de la compra.
	 * @param formaCompra Puede ser forma Online o Fisica.
	 * @param formaPago Es la forma de pago en Efectivo, con Tarjeta o Pay Pal.
	 * @param producto Es el producto de la compra.
	 * 
	 */
	public static void escribirCabeceraRegistroCompras(int idCompra, String fechaCompra, String tienda, String importe,  String formaCompra, String formaPago, String producto) {
		
		utilidades.FicherosLog.cabeceraComprasLog();
		
		utilidades.FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idCompra, fechaCompra, tienda, importe, formaCompra, formaPago, producto);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idGastoFijo Es el identificador del gasto fijo.
	 * @param fechaFactura Fecha de la factura del gasto.
	 * @param numFactura Es el numero de la factura del gasto.
	 * @param fechaInicio Es la fecha de inicio del consumo del gasto fijo.
	 * @param fechaFin Es la fecha fin del consumo del gasto fijo.
	 * @param tipoGasto Es el tipo de gasto como la luz, el gas, el agua, el telefono, etc.
	 * @param importe Es el importe del gasto fijo.
	 * 
	 */
	public static void escribirCabeceraRegistroGastosFijos(int idGastoFijo, String fechaFactura, String numFactura, String fechaInicio,  String fechaFin, String tipoGasto, String importe) {
		
		utilidades.FicherosLog.cabeceraGastoFijoLog();
		
		utilidades.FicherosLog.escribirGastoFijoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idGastoFijo, fechaFactura, numFactura, fechaInicio, fechaFin, tipoGasto, importe);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idPresu Es el identificador del presupuesto.
	 * @param fechaPresu Fecha del presupuesto.
	 * @param tipo Es el tipo de presupuesto, Semanal o Mensual.
	 * @param presupuesto Es la cuantia del presupuesto.
	 * 
	 */
	public static void escribirCabeceraRegistroPresus(int idPresu, String fechaPresu, String tipo, String presupuesto) {
		
		utilidades.FicherosLog.cabeceraPresuLog();
		
		utilidades.FicherosLog.escribirPresuLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idPresu, fechaPresu, tipo, presupuesto);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idGastoFijo Es el identificador del ingreso
	 * @param fechaIngreso Fecha del ingreso
	 * @param concepto Es el concepto del ingreso.
	 * @param importe Es el importe del ingreso.
	 * 
	 */
	public static void escribirCabeceraRegistroIngresos(int idIngreso, String fechaIngreso, String concepto, String importe) {
		
		utilidades.FicherosLog.cabeceraIngresoLog();
		
		utilidades.FicherosLog.escribirIngresoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idIngreso, fechaIngreso, concepto, importe);
		
	}
	
	public static boolean compararUltimaLineaConCabeceraCompras(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_COMPRAS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConCabeceraGastosFijos(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_GASTOS_FIJOS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConCabeceraPresu(String ultimaLinea) {

		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_PRESUPUESTOS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConRaya(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_RAYA)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	/**
	 * Agrega los datos de la compra a la lista de la compra.
	 * 
	 * @param idCompra Es el identificador de la compra.
	 * @param fechaCompra Fecha de la compra.
	 * @param tipoCompra Es el tipo de compra que se ha realizado, como comida, pescaderia, famarcia, etc.
	 * @param tienda Tienda efectuado la compra.
	 * @param importe El importe de la compra en decimales.
	 * @param formaCompra Puede ser forma Online o Fisica.
	 * @param formaPago Es la forma de pago en Efectivo, con Tarjeta o Pay Pal.
	 * @param producto Es el producto de la compra.
	 * 
	 */
	public static void agregarCompraALista(int idCompra, String fechaCompra, String tienda, double importe, String formaCompra, String formaPago, String producto, Compras listaCompras) {
		
		Compra compra = new Compra(idCompra, fechaCompra, tienda, producto, importe, formaCompra, formaPago);
		
		// Se agrega cada objeto a la lista
		listaCompras.anadir(compra);
		
	}
	
	/**
	 * Obtiene los datos de la lista para cargar el JComboBox a mostrar.
	 * 
	 * @param lista Es la lista en String que viene cargadas con los nombres de la tienda.
	 * @param comboBox Es el JComboBox a cargar segun la lista
	 * @return comboBox Devuelve un JComboBox de String para mostrar en la vista. Si la lista viene vacia sera nulo.
	 * 
	 */
	public static JComboBox<String> getCombo(ArrayList<String> lista, JComboBox<String> comboBox) {
		
		if(!utilidades.UtilesValida.esNula(lista)){
			for(int i=0;i<lista.size();i++) {
				comboBox.addItem(lista.get(i));
			}
		}
		else {
			comboBox = null;
		}
		
		return comboBox;
	}
	
	/**
	 * Obtiene los datos de la lista para cargar el JComboBox a mostrar.
	 * 
	 * @param lista Es la lista en String que viene cargadas con los nombres de la tienda.
	 * @param comboBox Es el JComboBox a cargar segun la lista
	 * @param nombreItem Es el nombre de item a buscar en la lista a devolver.
	 * @return comboBox Devuelve un JComboBox de String para mostrar en la vista. Si la lista viene vacia sera nulo.
	 * 
	 */
	public static JComboBox<String> getItem(ArrayList<String> lista, JComboBox<String> comboBox, String nombreItem) {
		
		if(!utilidades.UtilesValida.esNula(lista)){
			for(int i=0;i<lista.size();i++) {
				
				if(lista.get(i).equals(nombreItem)) {
					comboBox.setSelectedItem(nombreItem);
				}
			}
		}
		else {
			comboBox = null;
		}
		
		return comboBox;
	}
	
	/**
	 * Obtiene los datos del archivo JSON.
	 * 
	 * @param modelo Es el modelo asociado a la lista para obtener los datos del archivo JSON especifico.
	 * @param listaString Es una lista de tipo String para cargar los datos del JSON.
	 * @return listaString Devuelve una lista cargada de datos, sino es que no se puede leer el archivo o no se puede parsear el JSON.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<String> getLista(String modelo){
		
		String json = null;
		ArrayList<String> listaString = new ArrayList<String>();
		
		try {
			json=JSON.readfile(Constantes.RUTA_SRC_ARCHIVO_JSON+modelo+Constantes.EXTENSION_JSON);
			
			if(!utilidades.UtilesValida.esNulo(json)) {
				listaString = JSON.parseJSON(json, Constantes.CAMPO_JSON_NOMBRE);
			 	//listaFormaCompra = JSON.leerFormaCompraJSON(json);
				
			}
			
		} catch (IOException | JSONException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		return listaString;
		
	}
	
	/**
	 * Parsea un String a una fecha de tipo Date.
	 * 
	 * @param cadena Es la fecha a formatear o a parsear.
	 * @return fecha Devuelve una fecha de tipo date si no sea ha dado ParseException.
	 * 
	 */
	public static Date parseaFecha(String cadena) {
    	
    	Date fecha = null;
         
		try {
			 SimpleDateFormat sdf = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO);
		     fecha = sdf.parse(cadena);
		     return fecha;
		} catch (ParseException e) {
		     return fecha;
		}
    }
	
	/**
	 * Se obtiene un array bidireccional para luego mostrar en la tabla.
	 * 
	 * @param lista Es una lista de string.
	 * @return arrayTabla Devuelve un array bidireccional relleno o nulo.
	 * 
	 */
	public static Object[][] obtenerArrayTabla(ArrayList<String> lista){
		
		Object[][] arrayTabla = null;
		
		if(!UtilesValida.esNula(lista)) {
			
			ArrayList<ArrayList<Object>> registros = FicherosRegistros.obtenerRegistros(lista);
			arrayTabla = new Object[registros.size()][];
			
			for (int i = 0; i < registros.size(); i++) {
			    ArrayList<Object> row = registros.get(i);
			    row.add(false);
			    arrayTabla[i] = row.toArray(new Object[row.size()]);
			}
		}
		
		return arrayTabla;
		
	}
	
	/**
	 * Se obtiene un array bidireccional para luego mostrar en la tabla.
	 * 
	 * @param lista Es una lista de string.
	 * @param fechaInicio Es la fecha de inicio de las compras concretas.
	 * @param fechaFin Es la fecha de fin de las compras concretas.
	 * @param tienda Es el nombre de una tienda seleccionada.
	 * @param listaOpcion Es una lista de opciones como 1 fecha, 2 fechas o tiendas.
	 * @return arrayTabla Devuelve un array bidireccional relleno o nulo.
	 * 
	 */
	public static Object[][] obtenerArrayTabla(ArrayList<String> lista, String fechaInicio, String fechaFin, String tienda, String formaCompra, String listaOpcion){
		
		Object[][] arrayTabla = null;
		int size = 0;
		int j=0;
		
		if(!UtilesValida.esNula(lista)) {
			
			ArrayList<ArrayList<Object>> registros = FicherosRegistros.obtenerRegistros(lista);
			//arrayTabla = new Object[registros.size()][];
			arrayTabla = new Object[size][];
			
			for (int i = 0; i < registros.size(); i++) {
			    ArrayList<Object> row = registros.get(i);

			    if(!UtilesValida.esNulo(listaOpcion)) {
			    	
			    	if(listaOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[1])) { // 1 Fecha
			    		if(UtilesValida.esFecha(fechaInicio)) {
			    			if(row.get(1).equals(fechaInicio)) {
			    				
				    			size++;
				    			arrayTabla = (Object[][])resizeArray(arrayTabla, size);
			    				
	        			    	row.add(false);
	        			    	arrayTabla[j] = row.toArray(new Object[row.size()]);
	        			    	j++;
	        			    }
			    		}
			    		
			    	}
			    	else if(listaOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[2])) { // 2 Fechas
			    		if(UtilesValida.esFecha(row.get(1).toString()) && UtilesValida.esFecha(fechaInicio) && UtilesValida.esFecha(fechaFin)) {
			    					    			
			    			Date fecha 		= Utilidades.parseaFecha(row.get(1).toString());
			    			Date dateInicio = Utilidades.parseaFecha(fechaInicio);
			    			Date dateFin 	= Utilidades.parseaFecha(fechaFin);
			    			
			    			if(UtilesValida.betweenFechas(fecha, dateInicio, dateFin)) {
			    				
				    			size++;
				    			arrayTabla = (Object[][])resizeArray(arrayTabla, size);
			    				
			    				row.add(false);
	        			    	arrayTabla[j] = row.toArray(new Object[row.size()]);
	        			    	j++;
			    			}
			    		}
			    	}
			    	else if(listaOpcion.equals(ListaConstantesValidacion.LISTA_OPCIONES[3])) { // Tiendas
			    		
			    		if(UtilesValida.esNulo(formaCompra)) { // si la forma compra es nula
			    			
				    		if(row.get(2).equals(tienda)) {
				    			
				    			size++;
				    			arrayTabla = (Object[][])resizeArray(arrayTabla, size);
				    			
			    				row.add(false);
	        			    	arrayTabla[j] = row.toArray(new Object[row.size()]);
	        			    	j++;
				    		}
			    		}
			    		else if(!UtilesValida.esNulo(formaCompra)) {
			    			
				    		if( row.get(2).equals(tienda) && row.get(5).equals(formaCompra) ) {
				    			
				    			size++;
				    			arrayTabla = (Object[][])resizeArray(arrayTabla, size);
				    			
			    				row.add(false);
	        			    	arrayTabla[j] = row.toArray(new Object[row.size()]);
	        			    	j++;
				    		}
			    		}
			    	}
			    	
			    }
			    else {
	    			size++;
	    			arrayTabla = (Object[][])resizeArray(arrayTabla, size);
	    			
    				row.add(false);
			    	arrayTabla[j] = row.toArray(new Object[row.size()]);
			    	j++;
			    }
			}
		}
		
		return arrayTabla;
		
	}
	
	/**
	* Reallocates an array with a new size, and copies the contents
	* of the old array to the new array.
	* @param array  the array to resize.
	* @param newSize   the new array size.
	* @return          The resized array.
	*/
	@SuppressWarnings("rawtypes")
	public static Object resizeArray(Object array, int newSize) {
		
	   int oldSize = java.lang.reflect.Array.getLength(array);
	   Class elementType = array.getClass().getComponentType();
	   Object newArray = java.lang.reflect.Array.newInstance(elementType,newSize);
	   int preserveLength = Math.min(oldSize,newSize);

	   if (preserveLength > 0) {
		  System.arraycopy(array,0,newArray,0,preserveLength);
	   }

	   return newArray;
	}
}
