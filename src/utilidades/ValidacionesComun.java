package utilidades;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ValidacionesComun {


	public static boolean validarCamposCompra(String fecha, String tienda, String producto, String importe, String formaCompra, String formaPago, JTextField txtProducto, JTextField txtCoste){
			
		boolean correcto=true;		
		
		if(UtilesValida.esNulo(fecha)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(UtilesValida.esNulo(tienda)){
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_TIENDA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		if(UtilesValida.esNulo(producto)){
			JOptionPane.showMessageDialog(txtProducto, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_PRODUCTO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_INCORRECTO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		if(UtilesValida.esNulo(importe)){
			JOptionPane.showMessageDialog(txtCoste, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_IMPORTE+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		else if (!utilidades.UtilesValida.esDoble(importe)){
			JOptionPane.showMessageDialog(null, Constantes.CAMPO_VISTA_IMPORTE+Constantes.ESPACIO+Constantes.ERROR_DEBE_SER_NUMERO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_INCORRECTO, JOptionPane.ERROR_MESSAGE);
		}
		
		if(UtilesValida.esNulo(formaCompra)){
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FORMA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		if(UtilesValida.esNulo(formaPago)){
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FORMA_PAGO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		return correcto;
	}
	

	public static boolean validarCamposGastosFijos(String fecha, String numFactura, String fechaInicio, String fechaFn, String tipoGasto, String importe, JTextField txtNumFactura, JTextField txtImporte){
			
		boolean correcto=true;		
		
		if(UtilesValida.esNulo(fecha)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA_DOC+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_DOC+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(UtilesValida.esNulo(numFactura)){
			JOptionPane.showMessageDialog(txtNumFactura, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_NUM_FACTURA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		
		if(UtilesValida.esNulo(fechaInicio)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA_INICIO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fechaInicio.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_INICIO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(UtilesValida.esNulo(fechaFn)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA_FIN+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fechaFn.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_FIN+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(UtilesValida.esNulo(tipoGasto)){
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_TIPO_GASTO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		if(UtilesValida.esNulo(importe)){
			JOptionPane.showMessageDialog(txtImporte, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_IMPORTE+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		else if(!UtilesValida.esDoble(importe)){
			JOptionPane.showMessageDialog(txtImporte, Constantes.CAMPO_VISTA_IMPORTE+Constantes.ESPACIO+Constantes.ERROR_DEBE_SER_NUMERO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_INCORRECTO, JOptionPane.ERROR_MESSAGE);
			correcto=false;
		}
		
		return correcto;
	}
	
 
    public static boolean validarCamposPresu(String fecha, String tipo, String presu, JTextField txtPresu){ 
            
        boolean correcto=true;
        
		if(UtilesValida.esNulo(fecha)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
        
		if(UtilesValida.esNulo(presu)){
			
			JOptionPane.showMessageDialog(txtPresu, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_PRESUPUESTO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		else if(!UtilesValida.esDoble(presu)){
			JOptionPane.showMessageDialog(txtPresu, Constantes.CAMPO_VISTA_PRESUPUESTO+Constantes.ESPACIO+Constantes.ERROR_DEBE_SER_NUMERO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_INCORRECTO, JOptionPane.ERROR_MESSAGE);
			correcto=false;
		}
        
		
		if(UtilesValida.esNulo(tipo)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_TIPO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
        
        return correcto; 
    }
    
    
    public static boolean validarCamposIngreso(String fecha, String concepto, String importe, JTextField txtConcepto, JTextField txtImporte){ 
            
        boolean correcto=true;
        
		if(UtilesValida.esNulo(fecha)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FECHA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/

		if(UtilesValida.esNulo(concepto)){
			
			JOptionPane.showMessageDialog(txtConcepto, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_CONCEPTO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
        
		if(UtilesValida.esNulo(importe)){
			
			JOptionPane.showMessageDialog(txtImporte, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_IMPORTE+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		else if(!UtilesValida.esDoble(importe)){
			JOptionPane.showMessageDialog(txtImporte, Constantes.CAMPO_VISTA_IMPORTE+Constantes.ESPACIO+Constantes.ERROR_DEBE_SER_NUMERO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_INCORRECTO, JOptionPane.ERROR_MESSAGE);
			correcto=false;
		}
		
        return correcto; 
    }
    
	public static boolean validarCamposComprasConcretasSoloFecha(String fecha){
		
		boolean correcto=true;		
		
		if(UtilesValida.esNulo(fecha)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.CAMPO_VISTA_FECHA_INICIO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		return correcto;
		
	}
	
	public static boolean validarCamposComprasConcretasEntreFechas(String fecha1, String fecha2){
		
		boolean correcto=true;		
		
		if(UtilesValida.esNulo(fecha1)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.CAMPO_VISTA_FECHA_INICIO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(UtilesValida.esNulo(fecha2)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.CAMPO_VISTA_FECHA_FIN+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		/*else if(!fecha.equals(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO)) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FECHA_FORMATO_INCORRECTO+Constantes.CAMPO_VISTA_FECHA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}*/
		
		if(fecha1.equals(fecha2)) {
			JOptionPane.showMessageDialog(null, Constantes.CAMPO_VISTA_FECHA_FIN+Constantes.ESPACIO+Constantes.I_GRIEGA_MINUS+Constantes.ESPACIO+Constantes.CAMPO_VISTA_FECHA_FIN+Constantes.PUNTO, Constantes.VALIDA_CAMPOS_IGUALES, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		return correcto;
		
	}
	
	public static boolean validarCamposComprasConcretasSoloTienda(String tienda){
		
		boolean correcto=true;
		
		if(UtilesValida.esNulo(tienda)){
			
			JOptionPane.showMessageDialog(null, Constantes.AVISO_CAMPO_VACIO+Constantes.CAMPO_VISTA_TIENDA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		return correcto;
		
	}

	
}
